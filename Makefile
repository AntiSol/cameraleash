.PHONY: dist test clean

release:
	@echo "---BUILD RELEASE---"
	@#This will build a release by editing the version, building a distribution,
	@#	and making a copy of that distribution as archive/dist-$version.tar.bz2.
	@#	we also copy dist/latest.tar.bz2 to archive/$version.tar.bz2
	@#cd dist; tar jcvf /tmp/dist.tar.bz2 --exclude=tarballs cameraleash
	@./dist/do_release.sh
	

dist: clean config
	@echo "---BUILD DISTRIBUTION---";
	@./dist/build_dist.sh;
	@echo "You may want to 'make pnd' to build a new PND."

pnd:
	@echo "---BUILD PND---"
	@cd dist/; make pnd

test: clean config
	@echo "---RUN TESTS---";
	@python runtests.py;

config: 
	@echo "---BUILD CONFIG---";
	@echo -n "VERSION=" | cat - VERSION > version.py
	@echo " - write 'version.py'."
	

clean:
	@echo "---CLEANUP---";
	@#remove .pyc:
	@find . -maxdepth 2 -name \*.pyc -exec rm -v {} \;
	@#remove dist files:
	@# this is done by build_dist.sh
	@#rm -Rvf dist/cameraleash/*;
	@#the mkdir is done by build_dist.sh
	@#mkdir -vp dist/cameraleash/tarballs;
	
