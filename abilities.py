

import gphoto2 as gp

import logging

class AbilityMap(object):
	"""
	A class which unifies camera abilities (operations, file_operations, 
		and folder_operations) into one, with mapping to the appropriate
		gphoto abilities consts
	"""
	
	#a map containing our operation variable names and how they map into
	# gphoto abilities
	operations_map = {
		#operations:
		'CAPTURE_IMAGE': {
			'ability': 'operations',
			'map': gp.GP_OPERATION_CAPTURE_IMAGE,
			'desc': "Capture Images",
		},
		'CAPTURE_VIDEO': {
			'ability': 'operations',
			'map': gp.GP_OPERATION_CAPTURE_VIDEO,
			'desc': 'Capture Videos',
		},
		'CAPTURE_AUDIO': {
			'ability': 'operations',
			'map': gp.GP_OPERATION_CAPTURE_AUDIO,
			'desc': 'Capture Audio',
		},
		'CAPTURE_PREVIEW': {
			'ability': 'operations',
			'map': gp.GP_OPERATION_CAPTURE_PREVIEW,
			'desc': 'Capture Preview',
		},
		'CONFIG': {
			'ability': 'operations',
			'map': gp.GP_OPERATION_CONFIG,
			'desc': 'Configure Camera/Driver',
		},
		'TRIGGER_CAPTURE': {
			'ability': 'operations',
			'map': gp.GP_OPERATION_TRIGGER_CAPTURE,
			'desc': 'Trigger capture and wait for events',
		},
		#file operations:
		'FILE_DELETE': {
			'ability': 'file_operations',
			'map': gp.GP_FILE_OPERATION_DELETE,
			'desc': 'Delete Files',
		},
		'FILE_PREVIEW': {
			'ability': 'file_operations',
			'map': gp.GP_FILE_OPERATION_PREVIEW,
			'desc': 'Preview Files',
		},
		'FILE_RAW': {
			'ability': 'file_operations',
			'map': gp.GP_FILE_OPERATION_RAW,
			'desc': 'RAW Retrieval',
		},
		'FILE_AUDIO': {
			'ability': 'file_operations',
			'map': gp.GP_FILE_OPERATION_AUDIO,
			'desc': 'Audio Retrieval',
		},
		'FILE_EXIF': {
			'ability': 'file_operations',
			'map': gp.GP_FILE_OPERATION_EXIF,
			'desc': 'EXIF Retrieval',
		},
		#folder operations:
		'DELETE_ALL': {
			'ability': 'file_operations',
			'map': gp.GP_FOLDER_OPERATION_DELETE_ALL,
			'desc': 'Empty Folder',
		},
		'FILE_PUT': {
			'ability': 'folder_operations',
			'map': gp.GP_FOLDER_OPERATION_PUT_FILE,
			'desc': 'Upload Files',
		},
		'CREATE_DIR': {
			'ability': 'folder_operations',
			'map': gp.GP_FOLDER_OPERATION_MAKE_DIR,
			'desc': 'Create Directories',
		},
		'DELETE_DIR': {
			'ability': 'folder_operations',
			'map': gp.GP_FOLDER_OPERATION_REMOVE_DIR,
			'desc': 'Delete Directories',
		}
	}
	
	def __init__(self,abilities):
		#create a bunch of class variables
		# with names defined in operations
		# e.g self.CAPTURE_IMAGE
		# these act as an enum
		
		v = 1
		for n in self.operations_map:
			setattr(self,n,v)
			self.operations_map[n]['value'] = v
			v += 1
			#v = v << 1
			
		self.abilities = abilities
			
	def can(self,ability):
		"""
		Returns a boolean indicating whether we have the specified ability
		ability may be the name of an ability as a string (i.e 'FILE_PUT')
		or the value of the corresponding instance variable (i.e self.FILE_PUT)
		"""
		if type(ability) == int:
			for n in self.operations_map:
				if self.operations_map[n]['value'] == ability:
					ability = n
					break
			
		ability = ability.upper()
		if not ability in self.operations_map:
			raise ValueError("Invlid ability value '%s'" % ability)
		
		itm = self.operations_map[ability]
		abilities = getattr(self.abilities,itm['ability'])
		
		return (abilities & itm['map']) != 0
		
	def describe(self,ability = None):
		"""
		Return an array describing abilities the device has. These will be 
			strings (the desc element of operations_map)
		Optionally, you can specify one of 'operations','file_operations', or
			'folder_operations' to describe just the abilities in that category
		"""
		if not ability:
			#get all:
			ret = []
			ret += self.describe('operations')
			ret += self.describe('file_operations')
			ret += self.describe('folder_operations')
			return ret
			
		if not hasattr(self.abilities,ability):
			raise ValueError("'%s' is not a valid ability type" % ability)
			
		ret = []
		abilities = getattr(self.abilities,ability)
		for n in self.operations_map:
			itm = self.operations_map[n]
			if itm['ability'] == ability:
				if abilities & itm['map']:
					ret.append(itm['desc'])
		return ret
		
