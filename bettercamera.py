

import gphoto2 as gp
import gtk
from gui import *

import logging

from config import ConfigMap
from abilities import AbilityMap

from helpers import *


import time
import datetime

#from scripting import ScriptEngine

from camerachooser import CameraChooserWindow

class BetterCamera(gp.Camera):
	"""
	An improved gphoto2 camera class
	"""
	def __init__(self,address = None):
		"""
		Create a Camera instance and initialise it using the provided address
		a valid address is returned by CameraChooserWindow.show 
			(or gp_camera_autodetect)
		"""
		gp.Camera.__init__(self)
		
		#lock for making things threadsafe. 
		#Do updates in a 'with self.lock' block
		#self.lock = threading.RLock()
		
		self.name = None
		self.address = address
		
		if address:
			#find camera name:
			for name, addr in gp.check_result(gp.gp_camera_autodetect()):
				if addr == address:
						self.name = name
		else:
			#no address given, show chooser window:
			resp = CameraChooserWindow.show()
			if resp:
				self.name,self.address = resp
			else:
				return None
		
		logging.info("Connecting to camera at %s",self.address)
		
		port_info_list = gp.PortInfoList()
		port_info_list.load()
		idx = port_info_list.lookup_path(self.address)
		self.set_port_info(port_info_list[idx])
		
		#try:
		self.init()
		"""
		except Exception as e:
			msg = "Error Initialising Camera!\n\n%s: %s" % (e.__class__.__name__, e.message)
			GtkOKDialog.show(None,msg,"Error!")
			return None
		"""
		
		#_config holds the gphoto2 config object.
		# all access to this needs to be threadsafe.
		self.config = ConfigMap(self)
		
		self.config.on_change = self.config_changed
		
		#callbacks for handling events:
		
		#generic callback for all events
		self.on_event = None
		
		#specific types of events that come from camera.wait_for_event
		self.on_file_add = None
		self.on_folder_add = None
		self.on_capture = None
		
		#a synthetic event we create by polling the config regularly
		# there can be multiple on_config_change events at once, you'll
		# get one for each config item that changes.
		# youf on_config_change callback should accept two params - the
		# name of the item, and the new value
		self.on_config_change = None
		
		#list of config widgets. to update when their config item changes
		self.configwidgets = []
		
		self.abilitymap = AbilityMap(self.get_abilities())
		
		self.infodump()
		
		
		#self.scripting = ScriptEngine(self)
		
		"""
		The filename format we use when savig files
		acepts datetime.strftime format strings and:
		$mode	mode (sequence/capture)
		$fn		filename	the filename from the camera
		"""
		self.filename_format = "%Y%m%d_%H%M%S_$mode_$fn"
		
		#we make AbilityMap constants available as camera consts,
		# to make life easier. So you can use e.g camera.can(camera.CAPTURE_IMAGE)
		for n in self.abilitymap.operations_map:
			setattr(self,n,self.abilitymap.operations_map[n]['value'])
		
	def process_filename(self,filename,mode = ""):
		"""
		Generate a filename string from self.filename_format
		"""
		ret = datetime.datetime.now().strftime(self.filename_format)
		
		if mode:
			ret = ret.replace('$mode',mode)
		else:
			ret = ret.replace('_$mode','')
			
		ret = ret.replace('$fn',filename)
		return ret
		
	def infodump(self):
		"""
		Dump info about the camera to the log
		"""
		logging.info("Camera Info for '%s'" % self.name)
		logging.info(" - Address: %s" % self.address)
		logging.info(" - Config: %s" % self.config.all)
		logging.info(" - Abilities: %s" % self.abilitymap.describe())
		
	
	def config_changed(self,item,value):
		#fire callback
		if self.on_config_change:
			self.on_config_change(item,value)
		
		#if we have a widget associated with the item, update it
		for w in self.configwidgets:
			#a change in any config item may affect the available choices
			# for another item, e.g changing focal length can change the
			# available apertures. So we update the range for all controls
			w.refresh_range()
			
			if w.item == item:
				logging.debug("Updating widget %s for '%s'" % (w, item))
				w.config_changed(value)
		
		
	def can_autofocus(self):
		cfg = self.config.all
		return 'focusmode' in cfg and 'autofocusdrive' in cfg and cfg['focusmode'] != "Manual"
		
	def autofocus(self):
		"""
		Do an auto focus.
		Note that this is probably going to be camera-specific. Initial
		 implementation works for Nikon D3200
		"""
		#get config once so that it doesn't refresh multiple times:'
		cfg = self.config.all
		
		if 'focusmode' in cfg and 'autofocusdrive' in cfg:
			if self.config['focusmode'] != "Manual":
				self.config['autofocusdrive'] = 1
			else:
				logging.warn("Autofocus is disabled!")
				return False
		else:
			logging.error("Cannot find autofocus config items. Unknown camera?")
			
		
	def reset(self):
		"""
		reset the camera. This is needed after certain operations
			(live preview, file download) to prevent horrific crashes
		"""
		#logging.info("Reset camera")
		self.exit()
		self.__init__(self.address)
		
	
	def __del__(self):
		self.exit()
	
	
	def capture(self,type = gp.GP_CAPTURE_IMAGE):
		"""
		An override to gphoto's capture() with a sensible default type
		"""
		if not self.can('CAPTURE_IMAGE'):
			logging.warn("Cannot capture image, device lacks ability")
			return None
		return gp.Camera.capture(self,type)
	
	
	def storage_summary(self):
		inf = self.get_storage_info()
		if inf:
			return "%s: %s of %s free" % (inf.label, 
					hunam_size(inf.freekbytes,True),
					hunam_size(inf.capacitykbytes,True)
			)
		else: 
			return ""
		
	
	def get_storage_info(self):
		"""
		Get camera storage information (free space, etc)
		"""
		#note that pop() may not be ideal here, perhaps there are cameras
		# with multiple card slots, and you get back a storageinfo for
		# each?
		ret = gp.check_result(gp.gp_camera_get_storageinfo(self))
		if ret:
			return ret.pop()
	
	
	
	def capture_and_download(self,dest,sequence_text = None):
		"""
		Capture an Image and download it to the local machine
		dest can be either a directory or a path and filename
		Returns the filename(s) saved
		NOTE: we want both jpeg and raw
		"""
		ret = []
		
		#try:
		i = self.capture()
		"""
		except Exception as ex:
			msg = "Error doing capture: %s %s." % (ex.__class__.__name__,ex.message)
			logging.error(msg)
			GtkOKDialog.show(None,msg + "\n\nThis happens when the camera can't autofocus.","Capture Error")
			return []
		"""
		
		src = os.path.join(i.folder,i.name)
		logging.info("Captured '%s'" % src)
		f = self.download_file(src,dest,True,sequence_text)
		if f:
			ret.append(f)
		
		#clear event queue. This may involve downloading another file
		# (if shooting in raw+jpg)
		evt,data = self.wait_for_event(200)
		while evt != gp.GP_EVENT_TIMEOUT:		
			#print("Event: %s, data: %s" % (event_text(evt),data))
			if evt == gp.GP_EVENT_FILE_ADDED:
				fn = os.path.join(data.folder,data.name)
				f = self.download_file(fn,dest,True,sequence_text)
				if f:
					ret.append(f)
				#self.download_file(fn)
			
			#try to grab another event
			evt,data = self.wait_for_event(200)
			
		
		return ret
		
	
	def abilities(self):
		"""
		Wrapper for AbilityMap.describe()
		"""
		return self.abilitymap.describe()
	
	def can(self,ability):
		"""
		Returns a boolean indicating whether thiis camera has the specified ability
			@see AbilityMap.can()
		"""
		return self.abilitymap.can(ability)
	
	@staticmethod
	def event_text(code):
		"""
		given a gp.GP_EVENT_X value, return a string with a descriptive name
		"""
		if code == gp.GP_EVENT_CAPTURE_COMPLETE:
			desc = "Capture Complete"
		elif code == gp.GP_EVENT_FILE_ADDED:
			desc = "File Added"
		elif code == gp.GP_EVENT_FOLDER_ADDED:
			desc = "Folder Added"
		elif code == gp.GP_EVENT_TIMEOUT:
			desc = "Timeout"
		else: #if code == gp.GP_EVENT_UNKNOWN:
			desc = "Unknown"
		return desc
		
	def dir(self,path = "/"):
		"""
		Get a directory listing for the specified path on the camera
		This will have subdirectories and files, both sorted, 
			directories first.
		Folders end with a slash
		Each entry will be a full path - you'll want to run basename
			on them for display
		"""
		if not path: path = "/"
		
		ret = []
		try:
			f = self.folder_list_folders(path)
			for itm in f:
				fn = os.path.join(path,itm[0]) + "/"	
				ret.append(fn)
			ret = sorted(ret)
			
			files = []
			f = self.folder_list_files(path)
			for itm in f:
				fn = os.path.join(path,itm[0])
				files.append(fn)
			files = sorted(files)
			ret += files
		except Exception as ex:
			logging.error(ex)
			
		return ret
		
	def file_get_info(self,filename):
		"""
		Override for gphoto's file_get_info to accept a full file path
		"""
		return gp.Camera.file_get_info(self,os.path.dirname(filename),os.path.basename(filename))
		
	def file_get(self,filename,type = gp.GP_FILE_TYPE_NORMAL):
		"""
		Override for file_get which takes a full file path and has a sensible default type
		"""
		return gp.Camera.file_get(self,os.path.dirname(filename),os.path.basename(filename),type)
		
	def file_read(self,filename,buffer,offset=0,type = gp.GP_FILE_TYPE_NORMAL):
		"""
		Override for file_read which takes a full file path and has a sensible default type
		"""
		return gp.Camera.file_read(self,os.path.dirname(filename),os.path.basename(filename),type,offset,buffer)
		
	def file_delete(self,filename):
		"""
		Override for file_delete to take a full path and checks abilities
		"""
		if not self.can('FILE_DELETE'):
			logging.warn("Cannot delete '%s', Device lacks FILE_DELETE ability!" % filename)
			return False
		return gp.Camera.file_delete(self,os.path.dirname(filename),os.path.basename(filename))
		
	def download_file(self,filename,dest,delete = False,mode=None):
		"""
		A simple 'download file' function.
		path is the full path of the camera file
		dest is where you want to save. If this is a directory, the 
			filename from the camera will be retained.
		
		if delete is true, the file will be deleted from the camera
		
		mode allows you to set the value of $mode when file is named
		
		returns the full path of the downloaded file 
			(i.e on the local machine)
			
		"""
		if os.path.exists(dest) and os.path.isdir(dest):
			#dest is a directory, keep the same filename and save there
			#dest = os.path.join(dest,os.path.basename(filename))
			dest = os.path.join(dest,self.process_filename(os.path.basename(filename),mode))
			
			#TODO: run vars through filename format to produce unique 
			# local filenames, reducing need to prompt about overwriting.
			
		if os.path.exists(dest):
			#dest file already exists
			resp,dest = GtkTextInputDialog.show(None,"File '%s' already exists!\n\nEnter a new filename, leave as-is to overwrite, \nor press cancel to abort download." % dest,dest,"File Exists")
			if not resp: 
				logging.info("User cancelled download of '%s', file exists" % dest)
				if delete:
					#delete the file from the camera anyway so that internal storage doesn't fill up'
					self.file_delete(filename)
					logging.info("Deleted '%s' from camera" % filename)
				return None #cancel press
			
		file = self.file_get(filename)
		if file:
			logging.info("Downloading '%s' to '%s'" % (filename,dest))
			try:
				file.save(dest)
				
				if delete:
					self.file_delete(filename)
					logging.info("Deleted '%s' from camera" % filename)
			
			except Exception as ex:
				msg = "Error Downloading file: %s - %s. Perhaps not enough space on disk?" % (ex.__class__.__name__,ex.message)
				logging.error(msg)
				GtkOKDialog.show(None,msg,"Error Downloading File")
				return None
				
			#self.reset()
			return dest
		else:
			logging.warn("Cannot download file '%s'!" % filename)
			return None
