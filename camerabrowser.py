
import gtk
from gui import *
import gphoto2 as gp

import logging

from gui import *

import logging


import time
import datetime


from helpers import *

class CameraFileBrowser(gtk.VBox):
	"""
	A Treeview for browsing camera files
	Will also have an actions pane to allow
	downloading/uploading files
	"""
	def __init__(self,parent):
		gtk.VBox.__init__(self)
		
		#the parent is the CamCtrlMainWindow. we need this so that 
		# .camera works
		# (we don't just keep a copy of camera because it can change
		#  if the user disconnects/reconnects)
		self.__parent = parent
		
		#root node for treeview
		self.root = None
		
		##################################
		# Load Icons for use in treeview:
		
		#use GTK theme icons for the file browser
		self.icons = gtk.icon_theme_get_default()
		
		"""
		
		useful gtk consts for self.icons.load_icon():
		
		gtk.STOCK_DIRECTORY
		STOCK_FILE
		STOCK_NETWORK
		STOCK_HARDDISK
		STOCK_FLOPPY
		STOCK_CDROM
		
		"""
		
		self.icon_size = 16
		
		#find_icon allows us to list a bunch of different icon names
		# in order of preference
		
		#icon for camera device (root)
		self.camera_icon = self.find_icon([
			'camera-photo',
			'camera',
			'drive-removable-media',
			'media-removable',
			'gnome-dev-removable',
			'drive-removable-media-usb',
			'gnome-dev-removable-usb',
			gtk.STOCK_HARDDISK
		])
		
		#icon for folders:
		self.folder_icon = self.icons.load_icon(gtk.STOCK_DIRECTORY,self.icon_size,0)
		
		#icon for non-image files
		self.file_icon = self.icons.load_icon(gtk.STOCK_FILE,self.icon_size,0)
		
		#icon for images
		self.image_icon = self.find_icon([
			'media-image',
			'image-jpeg',
			'gnome-mime-image-jpeg',
			'gnome-mime-image',
			'image',
			gtk.STOCK_FILE
		])
		
		"""
		There doesn't seem to be a reliably-available "loading" or 
			"busy" icon, so we'll just go with none.
		self.loading_icon = self.find_icon([
			'content-loading-symbolic',
			'loading',
			'busy'
		])
		"""
		
		#######################################
		# Set up controls:
		
		sw = gtk.ScrolledWindow()
		sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		self.pack_start(sw,True,True)	
		#columns: Icon, displayname, type, size, date, fullpath
		store = gtk.TreeStore(gtk.gdk.Pixbuf,str,str,str,str,str)
		self.store = store
		
		treeview = gtk.TreeView(store)
		treeview.connect('row-expanded',self.expanded)
		self.treeview = treeview
		
		sw.add(treeview)
		
		column = gtk.TreeViewColumn('Name')
		column.set_resizable(True)
		treeview.append_column(column)
		cell = gtk.CellRendererPixbuf()
		column.pack_start(cell, False)
		column.add_attribute(cell, 'pixbuf', 0)
		self.name_column = column
		
		#column = gtk.TreeViewColumn('Name')
		#treeview.append_column(column)
		cell = gtk.CellRendererText()
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 1)
		column.set_expand(True)
		
		column = gtk.TreeViewColumn('Type')
		treeview.append_column(column)
		cell = gtk.CellRendererText()
		column.pack_start(cell, False)
		column.add_attribute(cell, 'text', 2)
		
		column = gtk.TreeViewColumn('Size')
		treeview.append_column(column)
		cell = gtk.CellRendererText()
		column.pack_start(cell, False)
		column.add_attribute(cell, 'text', 3)
		
		column = gtk.TreeViewColumn('Modified')
		treeview.append_column(column)
		cell = gtk.CellRendererText()
		column.pack_start(cell, False)
		column.add_attribute(cell, 'text', 4)
		
		buttons = gtk.HButtonBox()
		self.pack_start(buttons,False,True,5)
		buttons.set_layout(gtk.BUTTONBOX_SPREAD)
		
		btn = gtk.Button()
		btn.set_label("Download")
		btn.connect('clicked',self.download)
		btn.set_sensitive(False)
		buttons.add(btn)
		self.download_button = btn
		
		btn = gtk.Button()
		btn.set_label("Refresh")
		btn.connect('clicked',self.refresh_node)
		#btn.set_sensitive(False)
		buttons.add(btn)
		self.refresh_button = btn
		
		self.selection = treeview.get_selection()
		self.selection.connect('changed',self.selection_changed)
		
		#setup initial view:
		self.reset()
		
	def selection_changed(self,widget = None):
		fn = self.current_file()
		if not fn:
			#nothing selected
			self.download_button.set_sensitive(False)
			return True
			
		if fn[-1] == "/":
			#directory selected
			self.download_button.set_sensitive(False)
			
		else:
			#file selected
			self.download_button.set_sensitive(True)
			
		
	def refresh_node(self,widget = None):
		"""
		Refresh button press event
		If a file is selected, the parent node is repopulated
		if a folder is selected, it is repopulated
		if nothing is selected, the root note is repopulated
		"""
		if not self.camera:
			return False
		
		#call exit on the camera so that we don't get cached filenames:
		# Nope, not needed, and causes problems.
		#self.camera.exit()
		
		model,node = self.treeview.get_selection().get_selected()
		if not node:
			#nothing selected, refresh the root:
			node = self.store.get_iter_root()
		else:
			fn = self.store.get_value(node,5)
			if fn[-1] != "/":
				#file, get parent
				node = self.store.iter_parent(node)
		
		#select the node we refreshed:
		self.treeview.get_selection().select_iter(node)
		
		self.populate_node(node)
		
		
	def download(self,widget = None):
		src = self.current_file()
		self.__parent.download_file(src)
		
		
	def get_camera(self):
		return self.__parent.camera
	camera = property(get_camera)
		
	def find_icon(self,names,size = None):
		"""
		Search for a list of icon names, returning the first match
		if size is not specified, self.icon_size will be used
		"""
		if not size:
			size = self.icon_size
			
		if type(names) != list:
			names = [names]
		
		for n in names:
			if self.icons.has_icon(n):
				logging.info("Using Icon '%s' for %s" % (n,names))
				return self.icons.load_icon(n,size,0)
				
		#not found:
		#return self.icons.load_icon(gtk.STOCK_MISSING_IMAGE,size,0)
		return None
			
	def expanded(self, treeview, iter, path):
		"""
		Callback for when a node is expanded
		"""
		if not self.camera:
			return False
			
		self.populate_node(iter)
	   
	def hunam_type(self,mimetype,filename):
		"""
		Returns a hunam-readable filetype based on mimetype 
			and possibly filename (if mimetype is unknown)
			
		Yes, this is kind of horrific, but I don't want to have to load a
		data file to figure this out.
		
		"""
		#logging.debug("Filename: %s, mimetype '%s'" % (filename,mimetype))
		
		#list of image MIME types (which I've modified) from: 
		# https://www.freeformatter.com/mime-types-list.html#mime-types-list
		
		if mimetype == 'image/bmp': return 'Bitmap Image'
		if mimetype == 'image/cgm': return 'Computer Graphics Metafile'
		if mimetype == 'image/g3fax': return 'G3 Fax Image'
		if mimetype == 'image/gif': return 'GIF Image'
		if mimetype == 'image/ief': return 'Image Exchange Format'
		if mimetype == 'image/jpeg': return 'JPEG Image'
		if mimetype == 'image/ktx': return 'OpenGL Texture'
		if mimetype == 'image/pjpeg': return 'JPEG Image (Progressive)'
		if mimetype == 'image/png': return 'PNG Image'
		if mimetype == 'image/svg+xml': return 'SVG Image'
		if mimetype == 'image/tiff': return 'TIFF Image'
		if mimetype == 'image/vnd.adobe.photoshop': return 'Photoshop Document'
		if mimetype == 'image/vnd.dece.graphic': return 'DECE Graphic'
		if mimetype == 'image/vnd.djvu': return 'DjVu'
		if mimetype == 'image/vnd.dvb.subtitle': return 'Subtitle File'
		if mimetype == 'image/vnd.dwg': return 'DWG Drawing'
		if mimetype == 'image/vnd.dxf': return 'AutoCAD DXF'
		if mimetype == 'image/vnd.fpx': return 'FlashPix'
		if mimetype == 'image/vnd.fujixerox.edmics-mmr': return 'EDMICS 2000'
		if mimetype == 'image/vnd.fujixerox.edmics-rlc': return 'EDMICS 2000'
		if mimetype == 'image/vnd.net-fpx': return 'FlashPix'
		if mimetype == 'image/vnd.wap.wbmp': return 'WAP Bitamp'
		if mimetype == 'image/vnd.xiff': return 'XIFF Image'
		if mimetype == 'image/webp': return 'WebP Image'
		if mimetype == 'image/x-cmu-raster': return 'CMU Image'
		if mimetype == 'image/x-cmx': return 'Corel Metafile Exchange (CMX)'
		if mimetype == 'image/x-freehand': return 'FreeHand MX'
		if mimetype == 'image/x-icon': return 'Icon Image'
		if mimetype == 'image/x-pcx': return 'PCX Image'
		if mimetype == 'image/x-pict': return 'PICT Image'
		if mimetype == 'image/x-png': return 'Portable Network Graphics (PNG) (x-token)'
		if mimetype == 'image/x-portable-anymap': return 'Portable Anymap Image'
		if mimetype == 'image/x-portable-bitmap': return 'Portable Bitmap Format'
		if mimetype == 'image/x-portable-graymap': return 'Portable Graymap Format'
		if mimetype == 'image/x-portable-pixmap': return 'Portable Pixmap Format'
		if mimetype == 'image/x-rgb': return 'Silicon Graphics RGB Bitmap'
		if mimetype == 'image/x-xbitmap': return 'X BitMap'
		if mimetype == 'image/x-xpixmap': return 'X PixMap'
		if mimetype == 'image/x-xwindowdump': return 'X Window Dump'
		
		
		#unknown mimetype, try looking at file extension:
		meh,ext = os.path.splitext(filename)
		ext = ext[1:].upper() #uppercase and remove period
		
		#RAW formats for various cameras:
		if ext in ['NEF', 'NRW']: return 'Nikon Raw Image'
		if ext == '3FR': return 'Hasselblad Raw Image'
		if ext == 'ARI': return 'Arri_Alexa Raw Image'
		if ext in ['ARW', 'SRF', 'SR2']: return 'Sony Raw Image'
		if ext == 'BAY': return 'Casio Raw Image'
		if ext == 'CRI': return 'Cintel Raw Image'
		if ext in ['CRW', 'CR2', 'CR3']: return 'Canon Raw Image'
		if ext in ['CAP', 'IIQ', 'EIP']: return 'Phase_One Raw Image'
		if ext in ['DCS', 'DCR', 'DRF', 'K25', 'KDC']: return 'Kodak Raw Image'
		if ext == 'DNG': return 'Adobe Raw Image'
		if ext == 'ERF': return 'Epson Raw Image'
		if ext == 'FFF': return 'Imacon/Hasselblad raw Raw Image'
		if ext == 'MEF': return 'Mamiya Raw Image'
		if ext == 'MDC': return 'Minolta, Agfa Raw Image'
		if ext == 'MOS': return 'Leaf Raw Image'
		if ext == 'MRW': return 'Minolta, Konica Minolta Raw Image'
		if ext == 'ORF': return 'Olympus Raw Image'
		if ext in ['PEF', 'PTX']: return 'Pentax Raw Image'
		if ext == 'PXN': return 'Logitech Raw Image'
		if ext == 'R3D': return 'RED Digital Cinema Raw Image'
		if ext == 'RAF': return 'Fuji Raw Image'
		if ext in ['RAW', 'RW2']: return 'Panasonic Raw Image'
		if ext in ['RAW', 'RWL', 'DNG']: return 'Leica Raw Image'
		if ext == 'RWZ': return 'Rawzor Raw Image'
		if ext == 'SRW': return 'Samsung Raw Image'
		if ext == 'X3F': return 'Sigma Raw Image'
		

		
		#movie formats:
		if ext == "MOV": return "Quicktime Movie"
		if ext in [ "MPG", "MPEG"]: return "MPEG Movie"
		if ext == "AVI": return "AVI Movie"
		if ext == "MP4": return "MPEG-4 Movie"
		if ext == "SRT": return "Subtitle File"
		
		#audio formats:
		if ext == "WAV": return "WAV Audio"
		if ext == "MP3": return "MP3 Audio"
		if ext == "OGG": return "Ogg Audio"
		if ext == "FLAC": return "FLAC Audio"
		
		#some other common formats:
		if ext == "ZIP": return "Zip Archive"
		if ext == "GZ": return "GZip Archive"
		if ext == "TAR": return "Tar Archive"
		if ext == "7z": return "7-Zip Archive"
		if ext == "TXT": return "Text File"
		
		
		return "Unknown"
		
	def populate_node(self,node):
		"""
		Populate a directory node with sub-items
		"""
		
		#prevent recustion when expanding the newly populated node:
		if (hasattr(self,"populating") and self.populating) or not self.camera:
			return False
		self.populating = True
		
		#get the full path we're expanding'
		path = self.store.get_value(node,5)
		
		#we could call camera.exit to ensure we're not getting cached 
		# contents, allowing this method to be used as 'refresh', too
		# however this slows down the interface when re-expanding a node,
		# so instead we'll call camera.exit() in self.refresh()
		#self.camera.exit()
		
		logging.info("dir('%s')" % path)
		
		self.__parent.busy(True)
		
		#cid = self.__parent.statusbar.get_context_id("listing")
		#mid = self.__parent.statusbar.push(cid,"Listing contents of '%s'..." % path)
		
		self.__parent.status("Listing contents of '%s'..." % path)
		
		#we do a bunch of main iterations so the interface can update
		while gtk.events_pending():		
			gtk.main_iteration();
		
		#list contents of path
		contents = self.camera.dir(path)
		
		#empty the node:
		child = self.store.iter_children(node)
		while child:
			self.store.remove(child)
			child = self.store.iter_children(node)
		
		for f in contents:
			if f[-1] == "/":
				type="Directory"
				icon = self.folder_icon
				fn = f[0:-1]
				basename = os.path.basename(fn)
				size = ""
				mtime = ""
			else:
				type="File"
				icon = self.file_icon
				basename = os.path.basename(f)
				#get file info
				inf = self.camera.file_get_info(f)
				type = inf.file.type
				if type[0:5] == "image":
					icon = self.image_icon
				type = self.hunam_type(type,basename)
				
				"""
				#append width and height to type if we have them:
				if inf.file.width and inf.file.height:
					type += " (%1d x %1d)" % (inf.file.width,inf.file.height)
				"""
				
				size = hunam_size(inf.file.size)
				mtime = datetime.datetime.fromtimestamp(inf.file.mtime).strftime("%d %b %Y %H:%M:%S")
			
			
			new = self.store.append(node,[icon,basename,type,size,mtime,f])
			
			if type == "Directory":
				#add fake child item to allow expansion
				self.store.append(new,[None,"Loading...","","","",""])
		
		self.treeview.expand_row(self.store.get_path(node),False)
		
		self.name_column.queue_resize()
		
		#print contents
		
		self.__parent.busy(False)
		self.__parent.status()
		
		self.populating = False
		
	def current_file(self):
		"""
		Return the full path and filename of the currently selected 
			file/directory. If it ends with a slash, it's a directory
		"""
		model,node = self.treeview.get_selection().get_selected()
		if not node: return None
		return model.get_value(node,5)
		
	def reset(self):
		"""
		Reset the file browser back to its unpopulated state
		Meant to be called when the camera changes (and on init)
		"""
		self.store.clear()
		self.root = self.store.append(None,[self.camera_icon,"/","Directory","","","/"])
		self.store.append(self.root,[None,"Loading...","","","",""])
		
