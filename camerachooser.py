from gui import *
import gphoto2 as gp
from helpers import *

class CameraChooserWindow(GtkBetterDialog):
	"""
	A dialog that shows a list of connected cameras, allowing the user to 
	choose one
	
	Use CameraChooserWindow.show(), which handles creating/destroying the 
	window and whatnot and simply returns the camera address and name (or None)
	
	"""
	def __init__(self,parent = None):
		GtkBetterDialog.__init__(self, title="Choose Camera", parent=parent, 
			flags=gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT, 
			buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
			gtk.STOCK_OK, gtk.RESPONSE_ACCEPT)
		)
		self.set_size_request(400,200)
		
		self.set_response_sensitive(gtk.RESPONSE_ACCEPT,False)
		
		msg = "Choose a camera to connect to."
		
		if is_pandora() and not usb_host_enabled():
			msg += "\nYou must enable USB Host mode to connect to a camera"
		
		
		self.add_label(msg)
		
		sw = gtk.ScrolledWindow()
		sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
		treeview = gtk.TreeView()
		
		column = gtk.TreeViewColumn("Camera")
		treeview.append_column(column)
		cr = gtk.CellRendererText()
		cr.set_property('editable', False)

		column.pack_start(cr, True)
		column.add_attribute(cr, "text", 0)
		column.set_expand(True)
		
		column = gtk.TreeViewColumn("Address")
		treeview.append_column(column)
		cr = gtk.CellRendererText()
		cr.set_property('editable', False)

		column.pack_start(cr, True)
		column.add_attribute(cr, "text", 1)
		column.set_expand(False)
		
		self.store = gtk.ListStore(str,str)
		treeview.set_model(self.store)
		
		self.selection = treeview.get_selection()
		self.selection.connect('changed',self.selection_changed)
		self.selection.set_mode(gtk.SELECTION_SINGLE)
		
		treeview.connect("row-activated", self.row_activated)
		
		sw.add(treeview)
		self.treeview = treeview
		
		#self.add_row(sw)
		container = self.get_content_area()
		container.pack_start(sw,True,True)
		
		#this is where the chosen camera name and address are stored
		self.camera = None
		self.addr = None
		
		self.refresh()
		btn = gtk.Button("Refresh")
		btn.connect('clicked',self.refresh)
		self.add_row(btn)
		self.show_all()
		
	def selection_changed(self,selection):
		if selection.count_selected_rows() > 0:
			t,itr = selection.get_selected()
			self.camera = self.store.get_value(itr, 0)
			self.addr = self.store.get_value(itr, 1)
			self.set_response_sensitive(gtk.RESPONSE_ACCEPT,True)
		
	def row_activated(self, treeview, row, path):
		#double-clicking a row is the same as pressing OK
		self.response(gtk.RESPONSE_ACCEPT)
		
	def refresh(self,widget = None):
		self.store.clear()
		num=0
		self.set_response_sensitive(gtk.RESPONSE_ACCEPT,False)
		
		for name, addr in gp.check_result(gp.gp_camera_autodetect()):
			#camera_list.append((name, addr))
			self.store.append((name,addr))
			num+=1
			
		self.treeview.get_selection().unselect_all()
			
		if num == 1:
			#only one option, select it:
			self.selection.select_path(0)

		return True
		
		
	@staticmethod
	def show(parent = None):
		dlg = CameraChooserWindow(parent)
		resp = dlg.run()
		ret = None
		if resp == gtk.RESPONSE_ACCEPT and dlg.addr:
			ret=(dlg.camera,dlg.addr)
			#print "Chose camera: %s (%s)" % ret
		dlg.destroy()
		return ret
