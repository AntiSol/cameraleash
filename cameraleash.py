#!/usr/bin/env python

from gui import *

import gphoto2 as gp

import pango

#import gtk

import gobject

import logging

from subprocess import *

logging.basicConfig( # filename='/tmp/cameraleash.log',
	format='%(asctime)s: %(levelname)s: %(funcName)s: %(message)s', level=logging.INFO)
#TODO: maybe have a --debug-photo flag or config item that enables this?
#gp.check_result(gp.use_python_logging())


import math

import time
import datetime

import thread
import threading

import sys

import io

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import numpy

import json

import re

from helpers import *

ICON_FILE="icon.png"

#load app icon and set it as the default for gtk windows:
if os.path.exists(ICON_FILE):
	icon = gtk.gdk.pixbuf_new_from_file(ICON_FILE)
	gtk.window_set_default_icon(icon)

"""
If there's an updater script available, run it.
We run it in a separate process so that e.g http calls don't block.
Note that in the dev branch, the updater isn't found - it lives in 
dist/ and is packaged in when a dist/release is built.
We do this intentionally to prevent the updater from running in the 
dev branch because it might try to "update" changed development files
to the latest release version.
"""
if os.path.exists('updater.sh'):
	logging.info("Running updater.sh...")
	p = Popen(['/bin/bash', 'updater.sh'])


from abilities import AbilityMap	

from config import ConfigMap

from configwidget import ConfigWidget

from bettercamera import BetterCamera

from camerabrowser import CameraFileBrowser

from camerachooser import CameraChooserWindow

from leftpanecfg import CamCtrlLeftPaneConfigWidget

from scripting import *



class CamCtrlMainWindow(gtk.Window):
	def __init__(self):
		gtk.Window.__init__(self)
		
		self.is_pandora = False
		if is_pandora():
			"""
			Running on OpenPandora, start in fullscreen mode:
			"""
			self.fullscreen()
			
			# On pandora, we should check if USB Host is on
			# 	if not, run
			# /usr/pandora/scripts/op_usbhost.sh
			# 	to toggle it.
			# 
			# if:
			# lsmod | grep ehci_hcd
			# 	returns output, usb host is on
			#
			# In the end I decided to do this from the launcher
			#	script, it's simpler to do from bash and the launcher
			#'	will be the normal way to launch it on a pandora.			
			
		
		#polling interval in milliseconds:
		self.poll_interval = 1000
		
		logging.info("%s v%1.3f Started." % (APPNAME, VERSION))
		
		self.set_title("%s v%1.3f" % (APPNAME, VERSION))
		self.connect("destroy", self.quit) #gtk.main_quit)
		self.set_size_request(800, 480)
		self.set_position(gtk.WIN_POS_CENTER)
		
		logging.info("Library versions: gphoto2 v%s; gphoto2_ports v%s; python-gphoto2 v%s" % (
			" ".join(gp.version.gp_library_version(gp.version.GP_VERSION_VERBOSE)),
			" ".join(gp.version.gp_port_library_version(gp.version.GP_VERSION_VERBOSE)),
			gp.__version__
		))
		
		#state variables. these are booleans indicating the state of the
		# program, to indicate e.g busynness
		self.capturing_sequence = self.camera_busy = self.previewing = False
		
		#default camera controls. This seems to me to be a sensible 
		# set of controls for my Nikon D3200
		self.camera_control_config = [
			{
				"item": "shutterspeed",
				"label": "Shutter Speed",
				"type": "slider"
			},
			{
				"item": "f-number",
				"label": "F-Number",
				"type": "slider"
			},
			{
				"item": "iso",
				"label": "ISO Speed",
				"type": "dropdown"
			},
			{
				"item": "focallength",
				"label": "Focal Length",
				"type": "slider"
			},
			{
				"item": "whitebalance",
				"label": "WhiteBalance",
				"type": "dropdown"
			},
			{
				"item": "exposurecompensation",
				"label": "EV",
				"type": "slider"
			},
			{
				"item": "exposuremetermode",
				"label": "Exposure Metering Mode",
				"type": "dropdown"
			},
			{
				"item": "focusmetermode",
				"label": "Focus Metering Mode",
				"type": "dropdown"
			},
			{
				"item": "capturetarget",
				"label": "Capture Target",
				"type": "dropdown"
			},
			{
				"item": "imagequality",
				"label": "Image Quality",
				"type": "dropdown"
			},
			{
				"item": "capturemode",
				"label": "Still Capture Mode",
				"type": "dropdown"
			},
			{
				"item": "flashmode",
				"label": "Flash Mode",
				"type": "dropdown"
			},
			{
				"item": "batterylevel",
				"label": "Battery Level",
				"type": "textbox"
			}
		]
		
		default_dir = '/tmp/'
		
		#Local directory chooser widget:
		self.local_path = GtkFileChooserWidget("dir",default_dir,"Choose Local Directory")
		
		self.__format = "%Y%m%d_%H%M%S_$mode_$fn"
		
		self.load_config()
		
		#mainbox = everything else, status bar
		mainbox = gtk.VBox()
		
		#box has 2 panes: info/settings, and preview/actions/filebrowser
		box = gtk.HBox()
		
		#ctrlbox is the vbox which holds our sliders etc
		ctrlbox = gtk.VBox()
		ctrlbox.set_size_request(200,1)
		
		###############################################
		#camera settings:
		
		#Camera name and selection button:
		b = gtk.HBox()
		self.camera_label = gtk.Label("No Camera Connected")
		self.camera_label.set_alignment(0,0.5)
		attrs = pango.AttrList()
		attrs.insert(pango.AttrWeight(pango.WEIGHT_BOLD, start_index=0, end_index=-1))
		self.camera_label.set_attributes(attrs)
		b.pack_start(self.camera_label,True,True)
		self.choose_camera_button = gtk.Button("...")
		#self.choose_camera_button.set_use_stock(True)
		self.choose_camera_button.connect('clicked',self.choose_camera)
		b.pack_start(self.choose_camera_button,False,False)
		ctrlbox.pack_start(b,False,True)
		
		#ooooh, this is interesting - text boxes can have a progress bar!
		#self.local_path.textbox.set_progress_fraction(0.5)
		
		ctrlbox.pack_start(gtk.HSeparator(),False,False,5)
		
		##############################################
		
		sw = gtk.ScrolledWindow()
		sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
		
		self.camera_controls = gtk.VBox()
		
		viewport = gtk.Viewport()
		
		viewport.set_size_request(200,-1)
		viewport.add(self.camera_controls)
		sw.add(viewport)
		
		#sw.add_with_viewport(self.camera_controls)
		
		ctrlbox.pack_start(sw,True,True,5)
		
		box.pack_start(ctrlbox,True,True)
		
		
		ctrlbox.pack_end(self.local_path,False,False)
		ctrlbox.pack_end(GtkLeftAlignLabel("Local Directory:"),False,False)
		ctrlbox.pack_end(gtk.HSeparator(),False,False,5)
		
		
		"""
		ctrlbox.pack_start(self.camera_controls)
		
		#self.camera_controls = ctrlbox
		
		sw.add(ctrlbox)
		box.pack_start(sw,False,True,5)
		"""
		self.box = box
		
		##############################################
		# The main area has a notebook. one tab (the default) is 
		#  preview and actions. There's also a file browser for 
		#  the camera, config, and scripting tabs.
		notebook = gtk.Notebook()
		
		self.preview_pane = gtk.VBox()
		self.file_browser = CameraFileBrowser(self)
		
		self.options_pane = gtk.VBox()
		
		self.about_pane = gtk.VBox()
		
		title = gtk.HBox()
		
		i = GtkIcon(ICON_FILE,64)
		
		title.pack_start(i,False,False,10)
		
		tb = gtk.VBox()
		
		tt=gtk.Label()
		tt.set_markup("<span size='x-large' weight='heavy'>%s</span>" % APPNAME)
		tt.set_alignment(0,0)
		
		tv = gtk.Label()
		tv.set_markup("<i>Version %1.3f.</i>" % VERSION)
		tv.set_alignment(0,0)
		
		tb.pack_start(tt,False,True,5)
		tb.pack_start(tv,False,True,0)
		
		title.pack_start(tb,True,True,0)
		
		tc = GtkLeftAlignLabel("Copyright AntiSol, 2019. GNU GPL v2 License.")
		
		tb.pack_start(tc,True,True,0)
		
		self.about_pane.pack_start(title,False,True,5)
		
		bb = gtk.HButtonBox()
		bb.set_spacing(20)
		bb.set_layout(gtk.BUTTONBOX_SPREAD)
		
		bb.pack_start(gtk.LinkButton('https://antisol.org/cameraleash/',"Website"),True,True,25)
		bb.pack_start(gtk.LinkButton('https://gitlab.com/AntiSol/cameraleash','Source Code'),True,True,25)
		
		self.about_pane.pack_start(bb,False,True,25)
		
		tb = GtkMultiLineTextBox()
		
		try:
			releasenotes=read_file_str("RELEASES").replace("\n\n","\n")
		except Exception as ex:
			releasenotes="(error reading release notes)"
		
		tb.text(releasenotes)
		tb.readonly(True)
		
		
		self.about_pane.pack_start(tb,True,True,5)
		
		#self.script_editor = CameraScriptEditor(self)
		
		notebook.append_page(self.preview_pane,gtk.Label("Preview / Actions"))
		notebook.append_page(self.file_browser,gtk.Label("File Browser"))
		#notebook.append_page(self.script_editor,gtk.Label("Scripting"))
		notebook.append_page(self.options_pane,gtk.Label("Options"))
		notebook.append_page(self.about_pane,gtk.Label("About"))
		
		#select the first tab on startup:
		notebook.set_current_page(0)
		box.pack_start(notebook,True,True)		
		
		
		##############################################
		# Options pane
		
		#cfgitems - configuration of left pane (camera config items)
		cfgitms = gtk.VBox()
		#self.options_pane.pack_start(cfgitms,True,True,5)
		#cfgitms.pack_start(GtkLeftAlignLabel("Left Pane Configuration"),False,False)
		f = gtk.Frame("Left Pane Configuration")
		f.add(cfgitms)
		f.set_size_request(400,0)
		self.options_pane.pack_start(f,True,True,5)
		
		self.left_pane_config_widget = cfgitms
		
		
		#opts - general program options
		opts = gtk.VBox()
		opts.set_border_width(5)
		f = gtk.Frame("General Options")
		f.add(opts)
		self.options_pane.pack_start(f,False,True,5)
		
		
		#text box for entering filename format when saving
		# files from camera.
		# by default, we prefix date and capture mode to the filename
		# given by the camera. This should prevent overwriting previous
		# capture sessions, and allow easy sorting.
		# you could include a path separator here to automatically
		# create a directory structure, we will try to create recursively
		# if the path doesn't exist'
		f=gtk.Frame("Save Filename format:")
		opts.pack_start(f,False,False,3)
		b = gtk.VBox()
		f.add(b)
		
		
		self.filename_format = gtk.Entry()
		self.filename_format.set_text(self.__format)
		self.filename_format.connect('changed',self.filename_format_changed)
		b.pack_start(self.filename_format,False,True,3)
		#todo: attach change event here, load from / save to config.
		# vars: %fn - filename from camera. includes extension,
		#				should be last!
		# 		%Y-%M-%d - date format
		#		%mode - capture mode: e.g 'capture', 'sequence'
		
		
		
		b = gtk.HBox()
		btn = gtk.Button("Load Config")
		btn.connect('clicked',self.load_config_clicked)
		b.pack_start(btn,True,True,5)
		opts.pack_start(b,False,True,5)
		
		btn = gtk.Button("Save Config")
		btn.connect('clicked',self.save_config_clicked)
		b.pack_start(btn,True,True,5)
		#opts.pack_start(btn,False,True,5)
		
		
		
		##############################################
		# Preview / Actions area
		
		#preview / last captured image
		wrapper = gtk.EventBox()
		wrapper.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("#000"))
		
		self.preview = gtk.Image()
		#self.preview.set_from_icon_name('missing-image',128)
		wrapper.add(self.preview)
		self.preview_pane.pack_start(wrapper,True,True,3)
		
		btns = gtk.HButtonBox()
		btns.set_layout(gtk.BUTTONBOX_SPREAD)
		
		btn = gtk.Button("Autofocus")
		btns.add(btn)
		self.autofocus_button = btn
		btn.connect("clicked",self.autofocus)
		
		btn = gtk.ToggleButton("Preview")
		btn.set_size_request(1,60)
		btn.connect('clicked',self.live_preview)
		btns.add(btn)
		self.preview_button = btn
		
		btn = GtkMarkupButton("<b>Capture</b>")
		btn.set_size_request(1,60)
		btn.connect('clicked',self.capture)
		btns.add(btn)
		self.capture_button = btn
		
		
		c = gtk.VBox()
		btn = gtk.ToggleButton("Capture Sequence")
		c.pack_start(btn)
		btn.connect('clicked',self.capture_sequence)
		self.capture_sequence_button = btn
		opts = gtk.Table(2,2)
		opts.attach(GtkLeftAlignLabel("Count:"),0,1,0,1)
		opts.attach(GtkLeftAlignLabel("Interval:"),0,1,1,2)
		self.seq_count = gtk.SpinButton(None,1)
		self.seq_count.set_numeric(True)
		self.seq_count.set_range(2, 1000000)
		self.seq_count.set_increments(1, 10)
		self.seq_count.set_value(3)
		self.seq_count.set_snap_to_ticks(True) 
		self.seq_count.set_wrap(True)
		opts.attach(self.seq_count,1,2,0,1)
		
		self.seq_interval = gtk.SpinButton(None,0.1,1)
		self.seq_interval.set_numeric(True)
		self.seq_interval.set_range(0.1, 86400.0)
		self.seq_interval.set_increments(0.1, 1)
		self.seq_interval.set_value(3.0)
		self.seq_interval.set_snap_to_ticks(True) 
		self.seq_interval.set_wrap(True)
		opts.attach(self.seq_interval,1,2,1,2)
		
		
		
		c.pack_start(opts,False,False,3)
		btns.add(c)
		
		self.preview_pane.pack_start(btns,False,True,5)
		
		#########
		mainbox.pack_start(box,True,True,0)
		
		# statusbar:
		self.statusbar = gtk.Statusbar()
		
		self.statusbar_label = gtk.Label()
		self.statusbar.pack_start(self.statusbar_label,True,True,3)
		self.statusbar_label.set_alignment(0,0.5) # GtkLeftAlignLabel()
		self.storage_label = gtk.Label()
		self.statusbar.pack_start(self.storage_label,False,False,3)
		
		mainbox.pack_start(self.statusbar, False, True, 0)
		
		
		self.add(mainbox)
		
		self.camera = None
		
		self.level = 0
		
		
		
		self.choose_camera()
		
		#don't screw around with threads and locks, instead we'll just
		# use a gobject timeout:
		self.update_thread = gobject.timeout_add(self.poll_interval,self.update_thread_func)
		logging.info("Polling every %1dms" % self.poll_interval)
		
		#run an iteration of the update thread immediately:
		self.update_thread_func()
		
		
		self.show_all()
		
		gtk.main()
		
	def filename_format_changed(self,widget):
		self.__format = widget.get_text()
		self.camera.filename_format = widget.get_text()
		
		
	def config_file(self):
		"""
		Returns the name of the default config file we load/save
		"""
		return os.path.expanduser("~/.cameraleash.conf")
		
	@staticmethod
	def _byteify(data, ignore_dicts = False):
		"""
		convert unicode dict returned by json into strings
		from: https://stackoverflow.com/questions/956867/how-to-get-string-objects-instead-of-unicode-from-json
		"""
		# if this is a unicode string, return its string representation
		if isinstance(data, unicode):
			return data.encode('utf-8')
		# if this is a list of values, return list of byteified values
		if isinstance(data, list):
			return [ CamCtrlMainWindow._byteify(item) for item in data ]
		# if this is a dictionary, return dictionary of byteified keys and values
		# but only if we haven't already byteified it
		if isinstance(data, dict) and not ignore_dicts:
			return {
				CamCtrlMainWindow._byteify(key): CamCtrlMainWindow._byteify(value)
				for key, value in data.iteritems()
			}
		# if it's anything else, return it in its original form
		return data
		
	def load_config(self,filename = None):
		if not filename:
			filename = self.config_file()
		
		if not os.path.exists(filename):
			logging.warn("Could not load config, '%s' does not exist" % filename)
			return False
		
		with open (filename, "r") as myfile:
			config = self._byteify(json.load(myfile))
		
		if 'controls' in config:
			self.camera_control_config = config['controls']
		
		if 'local_path' in config:
			self.local_path.val(config['local_path'])
		
		if 'filename_format' in config:
			self.__format = config['filename_format']
		
		logging.info("Loaded '%s'" % filename)
		
		
	def save_config(self,filename = None):
		if not filename:
			filename = self.config_file()
			
		config = {
			'controls': self.camera_control_config,
			'local_path': self.local_path.val(),
			'filename_format': self.__format,
		}
		
		data = json.dumps(config, sort_keys=True, indent=4, separators=(',', ': '))
		try:
			f = open(filename,'w')
			f.write("%s\n" % data)
			f.close()
			logging.info("Wrote '%s'" % filename)
			return True
		except Exception as ex:
			logging.error("Could not write config to '%s': %s - %s" % (filename, ex.__class__.__name__, ex.messgae))
			
		
	def load_config_clicked(self,widget = None):
		filename = self.config_file()
		dlg = gtk.FileChooserDialog(title="Choose config file to load", parent=self, 
			action=gtk.FILE_CHOOSER_ACTION_OPEN,
			buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,gtk.STOCK_OPEN, gtk.RESPONSE_ACCEPT)
		)
		dlg.set_filename(filename)
		dlg.set_current_folder_uri(os.path.dirname(filename))
		dlg.set_current_name(os.path.basename(filename))
		dlg.set_default_response(gtk.RESPONSE_ACCEPT)
		resp = dlg.run()
		if resp == gtk.RESPONSE_ACCEPT:
			self.load_config(dlg.get_filename())
			self.init_configwidget()
			self.init_controls()
		dlg.destroy()
		
		
	def save_config_clicked(self,widget = None):
		filename = self.config_file()
		dlg = gtk.FileChooserDialog(title="Save As...", parent=self, 
			action=gtk.FILE_CHOOSER_ACTION_SAVE,
			buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,gtk.STOCK_SAVE, gtk.RESPONSE_ACCEPT)
		)
		dlg.set_current_folder_uri(os.path.dirname(filename))
		dlg.set_filename(filename)
		dlg.set_current_folder_uri(os.path.dirname(filename))
		dlg.set_current_name(os.path.basename(filename))
		dlg.set_default_response(gtk.RESPONSE_ACCEPT)
		dlg.set_do_overwrite_confirmation(True)
		resp = dlg.run()
		if resp == gtk.RESPONSE_ACCEPT:
			self.save_config(dlg.get_filename())
		dlg.destroy()
		
	def autofocus(self,widget = None):
		self.camera.autofocus()
	
	def status(self,text=""):
		"""
		Set the status bar text.
		If called with no arguments, clears the status bar
		"""
		self.statusbar_label.set_label(text)
	
	def capture(self,widget = None, show_error_dialog = True, sequence_text=None):
		"""
		Capture an image, download it, and show it in the preview area 
			if it's a known filetype. 
		This may involve retreiving multiple files (raw+jpg)
		widget is provided by gtk callback (this method is connected to 
			the capture button)
		show_error_dialog determines behaviour on error. If true, the
			error will be shown in a dialog, otherwise it will be raised
			to the caller
		sequence_text is passed through to the filename formatter as 
		$mode, allows having e.g '1of10' in the file name
		"""
		if not self.camera: return False
		if not self.camera.can('CAPTURE_IMAGE'): # and not self.camera.can('TRIGGER_CAPTURE'):
			logging.warn("Cannot capture image, camera lacks ability!")
			return None
		
		if hasattr(self,'camera_busy') and self.camera_busy:	#prevent multiple calls
			return False
		self.camera_busy = True
		self.set_button_states()
		if widget:
			widget.set_sensitive(False)
		
		
		self.status("Capturing...")
		
		dest = self.local_path.val()
		self.busy(True)
		self.gui_update()
		
		#self.camera.trigger_capture()
		
		try:
			filenames = self.camera.capture_and_download(dest,sequence_text)
		except Exception as ex:
			#need to unset busy state:
			self.camera_busy = False
			self.busy(False)
			if widget:
				widget.set_sensitive(True)
			
			mid = self.status("Capture Error.")
			
			msg = "Error doing capture: %s %s." % (ex.__class__.__name__,ex.message)
			logging.error(msg)
			if show_error_dialog:
				GtkOKDialog.show(self,msg + "\n\nPerhaps the camera couldn't autofocus?","Capture Error")
			else:
				ex.message += ". Perhaps the camera couldn't autofocus?"
				raise ex
				
			return False
		
		
		self.status()
		
		if filenames:
			#we only want the basenames for display:
			bns = []
			for f in filenames:
				bns.append(os.path.basename(f))
			fns = "' & '".join(bns)
			self.status("Captured '%s'..." % fns)
			
		
		for filename in filenames:
			#if file is a supoprted type, show it in the preview:
			meh,ext = os.path.splitext(filename)
			ext = ext[1:].upper()
			#this is hacky - there's probably a way to ask PIL 
			# 	"Is this' a supported image?'
			if ext in ['BMP', 'DIB', 'DCX', 'EPS', 'PS', 'GIF', 'IM', 
					'JPG', 'JPE', 'JPEG', 'PCD', 'PCX', 'PNG', 'PBM', 
					'PGM', 'PPM', 'PSD', 'TIF', 'TIFF', 'XBM', 'XPM'
			]:
				try:
					img = Image.open(filename)
					self.to_preview(img,os.path.basename(filename))
				except Exception as ex:
					logging.error("Could not preview image - %s: %s" % (ex.__class__.__name__,ex.message))
				
				break
			
		self.camera_busy = False
		self.busy(False)
		if widget:
			widget.set_sensitive(True)
		self.set_button_states()
		
	def capture_sequence(self,widget = None):
		"""
		Capture a sequence of images. callback when "capture sequence" 
			is pressed
		"""
		
		#self.capture_sequence_button
		#self.seq_count
		#self.seq_interval
		
		#necessary to prevent this from firing when we set_active to false
		# on the button at the end of the sequence
		if hasattr(self,"capturing_sequence") and self.capturing_sequence:
			return False
		
		self.capturing_sequence = True
		self.set_button_states()
		self.gui_update()
		
		self.status("Capturing Sequence...")
		
		frames = 0
		
		errors = []
		
		next_frame = time.time()
		diff = 0
		
		try:
		
			while frames == 0 or self.capture_sequence_button.get_active():
				frames+=1
				self.gui_update()
				
				diff = time.time() - next_frame
				
				next_frame = time.time() + float(self.seq_interval.get_value())
				
				ok = True
				try:
					logging.debug("Capturing frame %1d, timing difference: %1.1f" % (frames,diff))
					if diff > 0.5:
						logging.warn("Capture is off by %1.1f seconds. This probably means the previous capture took longer than the desired interval." % diff)
					seqtext = "%1dof%1d" % (frames,self.seq_count.get_value())
					self.capture(None, False,seqtext)
				except Exception as ex:
					ok = False
					errors.append((frames,ex))
					logging.error("Error capturing sequence frame %1d: %s - %s" % (frames, ex.__class__.__name__, ex.message))
					
				self.gui_update()
				
				
				if frames >= int(self.seq_count.get_value()):
					break
				
				while time.time() < next_frame:
					
					if ok:
						self.status("Captured sequence %1d of %1d, next capture in %1.1f seconds" % (
								frames,self.seq_count.get_value(),next_frame - time.time())
						)
					else:
						self.status("Error capturing sequence %1d of %1d, next capture in %1.1f seconds" % (
								frames,self.seq_count.get_value(),next_frame - time.time())
						)
					
					if not self.capture_sequence_button.get_active():
						#user cancelled
						self.status("Cancelled sequence after %1d frames." % frames)
						break
					
					self.gui_update()
					time.sleep(0.1)
					
		except Exception as ex:
			logging.error("Sequence Exception: %s" % ex)
		finally:
			self.status("Sequence ended, %1d frames, %1d errors" % (frames,len(errors)))
			
			self.capture_sequence_button.set_active(False)
			self.capturing_sequence = False
			self.set_button_states()
			
					
	def set_button_states(self):
		"""
		Enables or disables button states according to camera capabilities
		and current state (capturing, etc)
		"""

		if not self.camera:
			capture = False
			focus = False
			preview = False
			sequence = False
		else:
			capture = self.camera.can('CAPTURE_IMAGE')
			focus = self.camera.can_autofocus()
			preview = self.camera.can('CAPTURE_PREVIEW')
			sequence = capture and (not self.camera_busy)
			
			if self.capturing_sequence or self.camera_busy or self.previewing:
				"""
				busy capturing
				"""
				capture = not self.camera_busy
				if self.capturing_sequence:
					#sequence toggle needs to be enabled so user can cancel
					sequence = True
					capture = False
				else:
					sequence = capture and (not self.camera_busy)
				
				if self.previewing:
					#preview needs to be enabled so it can be cancelled:
					preview = True
				else:
					preview = False
				
		
		self.capture_button.set_sensitive(capture)	
		
		self.capture_sequence_button.set_sensitive(sequence)
		self.seq_interval.set_sensitive(sequence)
		self.seq_count.set_sensitive(sequence)
		
		self.autofocus_button.set_sensitive(focus)
		
		self.preview_button.set_sensitive(preview)
		
	
	def to_preview(self,image,text = ""):
		"""
		scale the provided PIL image so that it fits in the preview 
			area, then convert it into a gdk pixbuf and load it into
			the preview widget
		If text is provided, it will be drawn on the image
			(or, to be more accurate, we'll try - this doesn't work on pandora)
		"""
		rect = self.preview.get_allocation()
		iw,ih = image.size
		aspect = iw/float(ih)
		#w = rect.width
		#h = rect.height
		
		if rect.width > rect.height:
			#viewport is wide - resize to height
			dh = rect.height
			dw = (dh*aspect)
			if dw > rect.width:
				dw = rect.width
				dh = dw/aspect
		else:
			#viewport is tall - resize to width
			dw = rect.width
			dh = (dw/aspect)
			if dh > rect.height:
				dh = rect.height
				dw = dh*aspect
		
		#display size
		ds = (dw * dh) / (iw*ih) * 100 
		
		#text to show
		msg = "%1dx%1d (%1.1f%%) %s" % (iw,ih,ds,text)
		
		#scale image to display
		image = image.resize((int(dw),int(dh)),Image.NEAREST) #using nearest for performance reasons, 4x faster than bilinear on pandora
		
		try:
			#try to draw helpful text.
			# This fails on the pandora (not enough memory?), 
			#	so it goes in a try block
			if not hasattr(self,'font'):
				fontfile = 'DejaVuSansMono.ttf'
				self.font = ImageFont.truetype(fontfile,14)
			#draw text
			draw = ImageDraw.Draw(image)
			th, tw = draw.textsize(msg,font = self.font)
			draw.text((0, (rect.height - (tw + 5))),msg,(255,255,0),font=self.font)
		except Exception as ex:
			#Show a warning that the font file might be missing, but only show it once:
			if not hasattr(self,"fontwarningshown"):
				logging.warn("Couldn't draw text on preview image. Is the font file (%s) missing? (Error: %s)" % (fontfile,ex))
				self.fontwarningshown=True
		
		#convert it to a pixbuf:
		arr = numpy.array(image)
		pb = gtk.gdk.pixbuf_new_from_array(arr, gtk.gdk.COLORSPACE_RGB, 8)
		
		#put it on the control:
		self.preview.set_from_pixbuf(pb)
		
		#cleanup. shouldn't be necessary.
		#del image
		#del arr
		
	def gui_update(self):
		"""
		Run some iterations of the gtk main loop to allow the gui to update
		"""
		while gtk.events_pending():
			gtk.main_iteration();
		
	def live_preview(self,widget = None):
		if hasattr(self,'camera_busy') and self.camera_busy:	#prevent multiple calls
			return False
		self.camera_busy = True
		self.previewing = True
		self.set_button_states()
		
		tm = time.time()
		
		self.status("Live Preview...")
		
		try:
			frames=0
			totalframes=0
			while frames == 0 or self.preview_button.get_active():
				totalframes+=1
				frames+=1
				file = self.camera.capture_preview()
				file_data = file.get_data_and_size()
				img = Image.open(io.BytesIO(file_data))
				self.to_preview(img,"Live Preview")
				
				del file_data
				del img
				
				#fps count:
				now = time.time()
				if int(tm) != int(now):
					elapsed = now - tm
					fps = frames / float(elapsed)
					self.status("Live Preview: %1.1f fps" % fps)
					tm = time.time()
					frames=0
				
				#update UI:
				self.gui_update()
				
		except Exception as ex:
			logging.warn("Live Preview Error: %s" % ex)
			
		finally:
			msg = "Live preview ended, %1d frames" % totalframes
			
			self.status(msg)
			logging.info(msg)
			
			
			#Close the mirror. Different cameras use different config 
			#	items for this.
			cfg = self.camera.config.all
			if 'viewfinder' in cfg:
				#Nikon
				self.camera.config['viewfinder'] = 0
			elif 'output' in cfg:
				#Canon
				self.camera.config['output'] = 'Off'
			else:
				#it seems that on some cameras a hack for this is to
				# capture an image and discare the result
				logging.warn("Couldn't close viewfinder!")
			
			#self.camera.reset()
			#self.init_camera()
			
			self.preview_button.set_active(False)
			self.previewing = False
			self.camera_busy = False
			self.set_button_states()
			
	def init_camera(self,address = None):
		"""
		Connects to a camera at the provided address and does several
		things in the gui that need to happen after a camera has been
		connected.
		if no address is specified, and we're currently connected 
			to a camera, this will disconnect from that camera and
			reconnect to it, reinitialising the connection. Do this
			with caution: it also resets e.g the filename output for
			captures back to capt0000.jpg
		"""
		addr = None
		if hasattr(self,'camera') and self.camera:
			"""
			Close existing camera connection properly
			"""
			self.camera.exit()
			#get the address so we can reconnect
			addr = self.camera.address
			#try to be clean
			del self.camera
		
		if address:
			addr = address
		
		try:
			self.camera = BetterCamera(addr)
		except Exception as e:
			msg = "Error Initialising Camera!\n\n%s: %s" % (e.__class__.__name__, e.message)
			logging.error(msg)
			GtkOKDialog.show(None,msg,"Error!")
			self.camera = None
			return False
		
		self.camera.on_config_change = self.camera_config_changed
		
		self.camera.filename_format = self.__format
		
		self.init_controls()
		
		self.init_configwidget()
		
		self.update_storage_summary()
		
		self.set_button_states()
		#self.enable_ability_actions()
		
		
	def camera_config_changed(self,item,value):
		if self.camera.can_autofocus():
			self.autofocus_button.set_sensitive(True)
		else:
			self.autofocus_button.set_sensitive(False)
			
		
	def enable_ability_actions(self):
		"""
		enable / disable action buttons depending on device capabilities
		"""
		
		if self.camera.can('CAPTURE_PREVIEW'):
			self.preview_button.set_sensitive(True)
		else:
			self.preview_button.set_sensitive(False)
		
		if self.camera.can('CAPTURE_IMAGE'): # or self.camera.can('TRIGGER_CAPTURE'):
			self.capture_button.set_sensitive(True)
			self.capture_sequence_button.set_sensitive(True)
		else:
			self.capture_button.set_sensitive(False)
			self.capture_sequence_button.set_sensitive(False)
		
	def init_controls(self):
		"""
		Add ConfigWidgets to self.camera_controls
		
		Also sets up the config widget for camera_controls
		
		"""
		
		#first, clear out any existing camera controls:
		for child in self.camera_controls.get_children():
			self.camera_controls.remove(child)
			child.destroy()
		
		if not self.camera: return False
		
		#empty the camera's list of configwidgets
		# so that it's not keeping a million useless references around
		# and firing a million useless callbacks.
		for w in self.camera.configwidgets:
			w.destroy()
		self.camera.configwidgets = []
		
		ctrls = self.camera_control_config
		
		cfg = self.camera.config.all
		
		for ctrl in ctrls:
			itm = ctrl['item']
			
			if itm not in cfg:
				#invalid item
				logging.debug("Skipping ConfigWidget for '%s', not in current camera config" % itm)
				continue
			
			if 'type' in ctrl:	
				type = ctrl['type']
			else: 
				type = 'textbox'
			
			if 'label' in ctrl:
				label = ctrl['label']
			else:
				label = None
			
			ctrl = ConfigWidget(self.camera,itm,type,label)
			ctrl.show_all()
			
			self.camera_controls.pack_start(ctrl,False,False)
		
	def init_configwidget(self):
		"""
		Set up the left pane config widget
		"""
		
		for child in self.left_pane_config_widget.get_children():
			self.left_pane_config_widget.remove(child)
		
		configurator = CamCtrlLeftPaneConfigWidget(self.camera,self.camera_control_config)
		configurator.on_change = self.left_pane_config_changed
		self.left_pane_config_widget.pack_start(
			configurator, True, True, 0
		)
		
		
	def left_pane_config_changed(self,widget):
		"""
		Callback for when the CamCtrlLeftPaneConfigWidget changes state
		"""
		#HACK: let a gui updte happen to allow some time for the rows to reorder.
		self.gui_update()
		
		self.camera_control_config = widget.new_config()
		
		self.init_controls()
		
		
	def quit(self,widget = None):
		
		#save config on exit
		self.save_config(self.config_file())
		
		if self.camera:
			self.camera.exit()
		
		self.update_thread = False
		gtk.main_quit()
		
		
		
	@staticmethod
	def event_text(event_type):
		if event_type == gp.GP_EVENT_CAPTURE_COMPLETE: return "Capture Complete"
		elif event_type == gp.GP_EVENT_FILE_ADDED: return "File Added"
		elif event_type == gp.GP_EVENT_FOLDER_ADDED: return "Folder Added"
		elif event_type == gp.GP_EVENT_TIMEOUT: return "Timeout"
		else: return "Unknown Event"
		
	def update_storage_summary(self):
		#refresh storage info:
		self.storage_label.set_text(self.camera.storage_summary())
		
	def update_thread_func(self):
		"""
		This function runs periodically in a separate thread, allowing 
		polling of the camera config for changes and events
		"""
		
		#not while previewing:
		if hasattr(self,'camera_busy') and self.camera_busy:	#prevent multiple calls
			return True
			
			
		try:
			
			logging.debug("Polling for changes...")
			if self.camera:
				#with self.lock:
				
				#refresh camera config. This will fire any callbacks for
				# changed config items
				#self.camera.config._config = self.camera.config.populate_map()
				self.camera.config.refresh()
				
				#handle pending camera events
				typ,data = self.camera.wait_for_event(1)
				while typ != gp.GP_EVENT_TIMEOUT:
					
					logging.info("update_thread_func: Event: %s (%1d), data: %s" % (self.event_text(typ),typ,data))
					
					#if re.match(r"^PTP Property .* changed$",data):
					#	self.camera.config.refresh()
					#	break
					
					"""
					#Perhaps we should prompt the user with 'do you want to download this new file?'
					if typ == gp.GP_EVENT_FILE_ADDED:
						fn = os.path.join(data.folder,data.name)
						logging.info("New file: %s" % fn)
						self.download_file(fn)
					"""
					
					
					#try to grab another event
					typ,data = self.camera.wait_for_event(1)
				
				#it's important that this happens after wait_for_event, because
				# if the camera is disconnected that will throw an error which
				# we can catch, whereas this will segfault and crash horribly
				self.update_storage_summary()
			
		
		except Exception as ex:
			#need to catch all errors in this func becuse if we don't
			# return true the timeout will be killed.'
			logging.warn("%s in update thread: %s" % ( ex.__class__.__name__,ex.message))
				
		finally:
			return True
		
	def busy(self,val = True):
		"""
		set or clear the hourglass mouse pointer to inidcate busy state
		val is whether we're busy or not (true sets hourglass, false clears)
		"""
		if (val):
			watch = gtk.gdk.Cursor(gtk.gdk.WATCH)
			self.window.set_cursor(watch)
			#this is kind of ideal from a certain perspective, but 
			# pretty obnoxious in terms of UX
			#self.set_sensitive(False)
			self.gui_update()
			
		else:
			self.window.set_cursor(None)
			#self.set_sensitive(True)
		
	def download_file(self,filename,dest = None):
		"""
		Download a file, giving some nice GUI feedback to indicate what's happening
		"""
		
		if not dest: 
			dest = self.local_path.val()
		
		self.status("Downloading '%s'..." % filename)
		self.busy(True)
		self.gui_update()
		fn = None
		try:
			fn = self.camera.download_file(filename,dest)			
		except Exception as ex:
			self.status("Error downloading '%s': %s %s" % (os.path.basename(filename), ex.__class__.__name__,ex.message))
			self.busy(False)
			return False
		if fn:
			#if the file is a known/supported image type, show it in the preview
			meh,ext = os.path.splitext(filename)
			ext = ext[1:].upper()
			if ext in ['BMP', 'DIB', 'DCX', 'EPS', 'PS', 'GIF', 'IM', 
					'JPG', 'JPE', 'JPEG', 'PCD', 'PCX', 'PNG', 'PBM', 
					'PGM', 'PPM', 'PSD', 'TIF', 'TIFF', 'XBM', 'XPM'
			]:
				try:
					img = Image.open(fn)
					self.to_preview(img,os.path.basename(fn))
					#del img
				except Exception as ex:
					logging.error("Could not preview image - %s: %s" % (ex.__class__.__name__,ex.message))

		
		self.busy(False)
		self.status()
		self.gui_update()
		
		if fn:
			logging.info("Saved '%s'" % fn)
			
			self.status("Saved '%s'..." % fn)
			self.gui_update()
		

	def choose_camera(self,widget = None):
		"""
		Pop up a "choose Camera" dialog
		"""
		#choose a new one
		ret = CameraChooserWindow.show(self)
		if ret:
			#first we disconnect existing camera
			if self.camera:
				self.camera.exit()
				del self.camera
			
			name, addr = ret
			#self.camera = BetterCamera(addr)
			
			self.init_camera(addr)
			
		if self.camera:
			self.camera_label.set_text(self.camera.name)
		else:
			self.camera_label.set_text("Not Connected")
		
		self.file_browser.reset()
	
		self.set_button_states()
		
		
		
if __name__ == "__main__":
	app = CamCtrlMainWindow()

#name,addr = CameraChooserWindow.show(None)
#cam = BetterCamera(addr)
