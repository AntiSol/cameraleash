
import gphoto2 as gp
import logging

class ConfigMap(object):
	"""
	Provides an easier interface to the gphoto2 config hierarchy
	
	use .config to get a dict of name => value pairs containing the 
		current config. This is refreshed whenever you access .config
		
	You can also get and set individual config items as ConfigMap['item']
	
	NOTE: DO NOT USE ConfigMap.config['item'] = 'value', as this 
		bypasses callbacks and whatnot. You can do:
			ConfigMap['item'] = 'value'
		or:
			cfg = ConfigMap.config
			cfg['item'] = 'value'
			ConfigMap.config = cfg
	
	"""
	
	"""
	For some reason, gphoto reports some properties as read-only when
		they are in fact writable. This is a list of these properties,
		they will be shown as writable regardless of what gphoto says
		
		edit: hmmm. Ok. so it seems that sometimes gphoto2 reports them
				as readonly and sometimes not. When it does report them
				as readonly, you can't set them. wtf?
		
	"""
	writable_items = [
		#'expprogram',
		#'focusmode',
	]
	
	def __init__(self,camera):
		#the gphoto2 config object
		self.__camera = camera
		
		#this is a callback we fire when a config item changes.
		# it should take 2 parameters: name, and value
		# this call back may fire multiple times in quick succession,
		# once for every item that has changed
		self.on_change = None
		
		#this is the map where we store name => value for all config items
		self.__config = {} # .__config needs to exist when we call populate_map
		#populate it with values from the camera:
		
		self.__config = self.populate_map()
		
	def __repr__(self):
		return "ConfigMap: %s" % str(self.config)
		
		
	def updateitem(self,item,value):
		if item not in self.__config.keys():
			logging.warning("Invalid config item '%s', not setting" % item)
			return False
		
		try:
			"""
			Cast to correct variable type depending on config item type:
			 
			 from help(gp.CameraWidget.set_value):
			 
			 Please pass (char*) for GP_WIDGET_MENU, GP_WIDGET_TEXT, GP_WIDGET_RADIO,
				(float) for GP_WIDGET_RANGE, (int) for GP_WIDGET_DATE, GP_WIDGET_TOGGLE,
				and (CameraWidgetCallback) for GP_WIDGET_BUTTON.
			"""
			cfg = self.__camera.get_config()
			itm = cfg.get_child_by_name(item)
			if (item not in self.writable_items) and itm.get_readonly():
				logging.warn("Cannot set readonly config item '%s'" % item)
				return False
				
			type = itm.get_type()
			if type in [ gp.GP_WIDGET_MENU, gp.GP_WIDGET_TEXT, gp.GP_WIDGET_RADIO ]:
				value = str(value)
			elif type in [ gp.GP_WIDGET_RANGE ]:
				value=float(value)
			elif type in [ gp.GP_WIDGET_DATE, gp.GP_WIDGET_TOGGLE ]:
				value = int(value)
				
			#TODO: support GP_WIDGET_BUTTON?
			
			if value == self.val(item):
				#no change
				return False
				
			logging.info("Trying to change item '%s' from '%s' to '%s'" % (item,self.__config[item],value))
			
			itm.set_value(value)
			itm.set_changed(True)
			self.__camera.set_config(cfg)
			
			#refresh config from camera. this takes care of firing callbacks
			self.refresh()
			return True
			
		except Exception as ex:
			logging.warning("Could not set new value '%s' for config item '%s' (%s)" % (value,item,ex))
			return False	
		
	def get_set_config(self,val = None):
		if val:
			if type(val) != dict:
				logging.warn("ConfigMap.update() must have a dict value provided")
				return False
			#find changed values:
			for idx in self.__config:
				if val[idx] != self.__config[idx]:
					#changed
					self.updateitem(idx,val[idx])
		#else:
		#	#if getting, refresh from the camera:
		#	self.refresh()
			
		return self.__config #.copy()
	config = property(get_set_config,get_set_config)
	#alias for .config, more natural when used as BetterCamera.config
	all = property(get_set_config)
	
	#you can access and set config items with ConfigMap['item']:
	def __getitem__(self, key):
		if key not in self.__config.keys():
			raise KeyError
		return self.config[key]
	def __setitem__(self, key, value):
		if key not in self.__config.keys():
			raise KeyError
		self.updateitem(key,value)
	
	def getitem(self,item):
		"""
		return a named gphoto2 config item, or none 
		"""
		try:
			item = str(item)
			cfg = self.__camera.get_config()
			itm = cfg.get_child_by_name(item)
			return itm
		except Exception as ex:
			return None
		
	def describe(self,item = None):
		"""
		Return the description of a named config item
		if no item is provided, returns a dict containing name and 
		description for all config items
		"""
		if not item:
			ret = {}
			for idx in self.config:
				ret[idx] = self.describe(idx)
			return ret
		
		itm = self.getitem(item)
		if not itm:
			return None
		return itm.get_label()
		
	def typeof(self,item = None,text = False):
		"""
		return the type of a named config item.
		If item is not provided, you'll get back a dict with the type 
			of all items
		If text is False, will be a gp.GP_WIDGET_X const
			if True, it will be a text string
		"""
		
		if not item:
			ret = {}
			for idx in self.config:
				ret[idx] = self.typeof(idx,text)
			return ret
		
		itm = self.getitem(item)
		if not itm:
			return None
		ret = itm.get_type()
		if text:
			ret = self.type_text(ret)
		return ret
		
	def val(self,item,cached = False):
		"""
		return the value of a named config item.
		This retrieves the value from the camera
		If the value has changed since the last time it was refreshed,
		  fires the on_change callback
		"""
		
		if cached and item in self.__config:
			return self.__config[item]
		
		itm = self.getitem(item)
		if not itm:
			return None
		val = None
		if itm.get_type() != gp.GP_WIDGET_SECTION:
			val = itm.get_value()
			if item in self.__config and self.__config[item] != val:
				#value has changed, fire callback
				logging.info("Config item '%s' has changed to '%s'" % (item,val))
				#fire callback:
				if self.on_change:
					self.on_change(item,val)
					
			self.__config[item] = val
		
		return val
		
	def choices(self,item = None):
		"""
		Return available choices for a named config item as an array
		if no item is provided, you'll get back a dict containing names 
			and choices for all menu and radio type options.
		"""
		
		if not item:
			ret = {}
			for idx in self.__config:
				type = self.typeof(idx)
				if type == gp.GP_WIDGET_MENU or type == gp.GP_WIDGET_RADIO:
					ret[idx] = self.choices(idx)
			return ret
		
		itm = self.getitem(item)
		if not itm or (itm.get_type() != gp.GP_WIDGET_RADIO and itm.get_type() != gp.GP_WIDGET_MENU):
			#choices is only valid for a radio or menu
			return None
		ret = []
		for v in itm.get_choices():
			ret.append(v)
		return ret
		
	def range(self,item = None):
		"""
		Return range informaton for a named config item of type 
			GP_WIDGET_RANGE
		bla bla if item isn't specified you get a hash of all
		returns None if the item isn't a range
		ranges are of the format [min,max,increment]
		"""
		if not item:
			ret = {}
			for idx in self.__config:
				if self.typeof(idx) == gp.GP_WIDGET_RANGE:
					ret[idx] = self.range(idx)
			return ret
			
		itm = self.getitem(item)
		if not itm:
			return False
		if itm.get_type() == gp.GP_WIDGET_RANGE:
			return itm.get_range()
		else:
			return None
		
	def writable(self,item = None):
		"""
		Return a boolean indicating whether a named config item is 
		writable. If False, item is readonly.
		If no item is provided you'll get a dict with name and value 
		for all config items
		"""
		if not item:
			ret = {}
			for idx in self.__config:
				ret[idx] = self.writable(idx)
			return ret
		
		if item in self.writable_items:
			"""
			writable_items overrides gphoto, just return true without
				bothering to query it
			"""
			return True
		
		itm = self.getitem(item)
		if not itm:
			return None
		return (not itm.get_readonly())
		
	@staticmethod
	def type_text(code):
		"""
		Given a GP_WIDGET_X value, return a string with a descriptive name
		"""
		if code == gp.GP_WIDGET_MENU:
			type = "Menu"
		elif code == gp.GP_WIDGET_TEXT:
			type = "Text"
		elif code == gp.GP_WIDGET_RADIO:
			type = "Radio"
		elif code == gp.GP_WIDGET_RANGE:
			type = "Range"
		elif code == gp.GP_WIDGET_DATE:
			type = "Date"
		elif code == gp.GP_WIDGET_TOGGLE:
			type = "Toggle"
		elif code == gp.GP_WIDGET_BUTTON:
			type = "Button"
		elif code == gp.GP_WIDGET_SECTION:
			type = "Section"
		elif code == gp.GP_WIDGET_WINDOW:
			type = "Window"
		else:
			type = "Unknown"
		return type
		
	def populate_map(self,config = None):
		"""
		returns a config map from a gphoto2 config item
		if not provided, gets the config from the camera
		"""
		ret = {}
		
		section=""
		if not config:
			logging.debug("Refreshing config from camera")
			config = self.__camera.get_config()
			
		children = config.get_children()
		for itm in children:
			
			typ = itm.get_type()
			if typ == gp.GP_WIDGET_SECTION:
				#populate children recursively:
				itms = self.populate_map(itm)
				ret.update(itms)
				continue
			
			name = itm.get_name()
			desc = itm.get_label()	
			val = itm.get_value()
			
			ret[name] = val
			
			
			#handle changes:
			if name in self.__config and self.__config[name] != val:
				#value has changed
				logging.info("Config item '%s' has changed to '%s'" % (name,val))
				
				#hack: set the value of the item before firing the callback
				
				self.__config[name] = val
				
				#fire callback:
				if self.on_change:
					self.on_change(name,val)
			
			
		return ret
		
	def refresh(self):
		"""
		Refresh the value of all config items
		"""
		self.__config = self.populate_map()
		
		#for itm in self.__config:
		#	self.__config[itm] = self.val(itm)
		
