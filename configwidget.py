
from gui import *

import gphoto2 as gp

class ConfigWidget(gtk.VBox):
	"""
	A control linked to a camera config item
	"""
	
	#a map of gphoto2 widget types, and the valid control types 
	# for each. The first item will be the default if no type is provided
	valid_types = {
		
		gp.GP_WIDGET_TEXT: ['textbox'],		#srring
		gp.GP_WIDGET_MENU: ['slider','dropdown', 'textbox' ],	#multiple choice
		gp.GP_WIDGET_RADIO: ['slider','dropdown', 'textbox' ],	#multiple choice
		gp.GP_WIDGET_RANGE: ['slider'],
		gp.GP_WIDGET_DATE: ['textbox'], # 'calendar', ?
		gp.GP_WIDGET_TOGGLE: ['checkbox','toggle'],
		#gp.GP_WIDGET_BUTTON: ['button']
		
	}
	
	type_descriptions = {
		'textbox': 'Text Box',
		'slider': 'Slider',
		#'radio': 'Radio Buttons',
		'dropdown': 'Drop-down List',
		#'calendar': 'Calendar',
		'checkbox': 'Check Box',
		'toggle': 'Toggle Button',
	}
	
	@staticmethod
	def valid_for(configtype,widgettype):
		"""
		Returns a boolean indicating whether the specified widget is OK
		 for use with the specified type of config item
		"""
		if not configtype in ConfigWidget.valid_types:
			return False
		return (widgettype in ConfigWidget.valid_types[configtype])
		
	@staticmethod
	def types_for(configtype):
		"""
		Returns a list of valid control types for the specified config type
		"""
		if not configtype in ConfigWidget.valid_types:
			return ['textbox']
		return ConfigWidget.valid_types[configtype]
		
	@staticmethod
	def default_for(configtype):
		"""
		Returns the default widget type for the specified config item type
		"""
		if not configtype in ConfigWidget.valid_types:
			return False
		return ConfigWidget.valid_types[configtype][0]
		
	@staticmethod
	def describe_type(widgettype):
		"""
		Returns a description of a widget type
		"""
		if widgettype not in ConfigWidget.type_descriptions:
			return "Unknown"
		return ConfigWidget.type_descriptions[widgettype]
	
	
	def __init__(self,camera,item,type = None,label = None):
		"""
		Create a new Camera widget
		camera is the BetterCamera object
		item is the name of the config item
		type is the type of control. If not provided, the default type
			will be used depending on the config item type.
			Note that this is restricted, you can only use certain 
			control types for certain config types. see valid_types to
			see which goes with which
		label is the label to use. If not provided, the label from the
			config item will be used
		"""
		gtk.VBox.__init__(self)
		self.set_size_request(200,-1)
		
		self.camera = camera
		
		if item not in camera.config.all:
			raise ValueError("Config item '%s' not found" % item)
		
		self.item = item
		
		#add this control to the camera's configwidgets so that it is
		# updated when config changes'
		camera.configwidgets.append(self)
		
		
		if not label:
			label = "%s:" % camera.config.describe(item)
		
		if not label:
			label = "%s:" % item
		
		self.item_type = self.camera.config.typeof(item)
		
		if not type or not self.valid_for(self.item_type,type):
			#default type for the config item type
			type = self.valid_types[self.item_type][0]
		
		signal = "changed"
		
		#create control:
		if type == "slider":
			if self.item_type == gp.GP_WIDGET_RANGE:
				#just a regular HScale
				self.control = gtk.HScale()
			else:
				self.control = GtkArrayScale()
			#need to set available values
			self.refresh_range()
			signal = "value-changed"
			
		elif type == "dropdown":
			self.control = GtkArrayDropdown()
			self.refresh_range()
		
		elif type == "checkbox":
			self.control = gtk.CheckButton()
			signal = 'toggled'
		
		elif type == "toggle":
			self.control = gtk.ToggleButton()
			signal = "toggled"
		
		else:
			#default to a textbox
			self.control = gtk.Entry()
		
		#readonly items cannpt be changed:
		if not self.camera.config.writable(self.item):
			self.control.set_sensitive(False)
		
		if isinstance(self.control,gtk.ToggleButton):
			#label goes on the control, not as a separate widget
			self.control.set_label(label)
		else:
			#label is a separate widget
			self.label = GtkLeftAlignLabel(label)
			self.pack_start(self.label,False,False)
		
		
		self.pack_start(self.control,True,True)
		
		self.pack_start(gtk.HSeparator(),False,True)
		
		#this is a flag that tells us we're updating from the config item
		# when this is true, the 'changed' callback will not try to 
		# update the config item
		self.updating = True
		
		self.refresh()
		
		self.__dirty = False
		
		self.control.connect(signal,self.control_changed)
		
		#lost-focus event handler:
		#self.control.connect('focus-out-event',self.lost_focus)
	
	def lost_focus(self,widget,evt):
		"""
		Callback for when the widget loses focus.
		We will avoid updating config on change, this causes lots of 
		traffic. instead, we'll fire 'change' here, when editing is done
		it would be nie to also have a timeout here, so that changes are
		applies after e.g 1 second of inactivity.
		"""
		if self.__dirty or (self.val() != self.camera.config[self.item]):
			#value has changed, try to update it:
			self.update_item()
			#change background colour to indicate we're trying to update:'
			#self.control.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("#FFF586"))
			self.__dirty = False
			
		
	def control_changed(self,widget = None):
		"""
		Callback for when control value changes
		"""
		if self.updating: 
			#we're updating from the config, don't try to update the item,
			# that could lead to an infinite loop
			return False
		self.update_item()
		
		
	def config_changed(self,value):
		"""
		Called when the config changes on the camera. This should do
		an update without trying to update the camera config
		"""
		self.refresh(value)
		
	def refresh(self,value = None):
		"""
		Update the control value with the value from the config item
		"""
		if value == None:
			#get from camera
			value = self.camera.config[self.item]
			
		self.updating = True
			
		#if the type is range, radio, or menu, the list of acceptable
		# values may have chnged
		self.refresh_range()
		
		self.val(value)
		#green to signify updated OK
		#self.control.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("#86FF8B"))
		self.updating = False
		
	def refresh_range(self):
		"""
		Update the list of available values if the config item is a
		range, menu, or radio
		"""
		if self.item_type not in [ gp.GP_WIDGET_MENU, gp.GP_WIDGET_RADIO, gp.GP_WIDGET_RANGE ]:
			return False
		
		#get data for possible values
		if self.item_type == gp.GP_WIDGET_RANGE:
			values = self.camera.config.range(self.item)
		else:
			values = self.camera.config.choices(self.item)
			
		#set possible choices:
		if isinstance(self.control,GtkArrayScale):
			#need to find the new index for the current value
			val = self.control.get_value()
			self.control.items = values
			
			try:
				self.control.set_value(val)
			except:
				#value not in new list, get value from camera
				val = self.camera.config.val(self.item)
				self.control.set_value(val)
				
		elif isinstance(self.control,gtk.Scale):
			self.control.set_adjustment(
				gtk.Adjustment(self.val(),values[0],values[1],values[2],values[2]*5)
			)
			
		elif isinstance(self.control,GtkArrayDropdown):
			self.control.items = values
			
		
	def update_item(self):
		"""
		Update the value of the config item with the value of the control
		"""
		ok = self.camera.config.updateitem(self.item,self.val())
		#we call updateitem because it returns bool indicating if 
		#update was successful or not.
		# we can use that to refresh val back to real value
		if not ok:
			#not chaned, read value back in from config
			self.refresh()
		
		
		
	def val(self,value = None):
		"""
		getter/setter for control value
		"""
		
		#NOTE: depending on what the control is, you might need to cast
		#		value to a certain type
		
		if isinstance(self.control,GtkArrayScale):
			if value != None:
				self.control.set_value(value)
			return self.control.get_value()
			
		elif isinstance(self.control,gtk.Scale):
			if value != None:
				value = float(value)
				self.control.set_value(value)
			return self.control.get_value()
			
		elif isinstance(self.control,GtkArrayDropdown):
			if value != None:
				self.control.set_value(value)
			#print "Value: %s" % self.control.get_value()
			return self.control.get_value()
			
		elif isinstance(self.control,gtk.ToggleButton):
			#this covers both checkbox and togglebutton, since checkbutton
			# is a subclass of togglebutton
			if value != None:
				self.control.set_active(value)
			
			return self.control.get_active()
			
		elif isinstance(self.control,gtk.Entry):
			#entry should be the last item in this elif block because
			# some other types are subclasses of it
			if value != None:
				value = str(value)
				self.control.set_text(value)
			return self.control.get_text()
			
	value = property(val,val)
	
