#!/bin/bash
#
# build_dist.sh
# Builds a release for the current codebase.

#we're in /dist, we need to switch to the project root
cd "`dirname $0`"
cd ../


#version file, where we get the app version
VERFILE=VERSION

#a file containing a list of files to be included in the release
DISTFILE=dist/dist_files.txt

#a file containing a list of files to be included in the www/
# release
WWWFILE=dist/www_files.txt

#location where we copy files to.
DIST="dist/cameraleash"

#location where we copy web files to.
WWW="dist/www"

#ensure it exists:
if [ ! -e "$DIST" ]; then mkdir -p "$DIST"; fi
if [ ! -e "$WWW" ]; then mkdir -p "$WWW"; fi
#clear out the destination:
rm -Rf $DIST/*
rm -Rf $WWW/*

#www/ needs a tarballs subdirectory that the
# apache user can write to, in order to generate
# differential tarballs:
mkdir -p $WWW/tarballs
chmod a+w $WWW/tarballs

#this file will be our version manifest
SUMFILE=`mktemp /tmp/XXXXXX`

#program version:
VER=`cat $VERFILE`
#first line of manifest is version:
echo $VER >>$SUMFILE

FL=""

echo "* Copying files..."

for f in `cat $DISTFILE`; do
	#get a checksum:
	
	SUM=`sha1sum "$f" | awk '{print $1}'`
	echo "$SUM  "`basename $f` >> $SUMFILE
	FL="$FL $f"
	echo -e "  - $f\t($SUM)"
	cp -R "$f" "$DIST/"
	#echo -en "  - "
	cp -R "$f" "$WWW/"
done

#now we copy web files.
# luckily, we don't need to checksum these
for f in `cat $WWWFILE`; do
	echo -en "  - "
	cp -vR $f "$WWW/"
done

#both dist_files and www_files have been copied,
# now we run custom_files.sh to do custom stuff.
if [ -e dist/custom_files.sh ]; then
	./dist/custom_files.sh
fi

#this is all replaced by the new www_files.txt system

#DISTDIR=`dirname $DIST`
#copy the updater script into the dist:
#echo -en "  - "
#cp -v $DISTDIR/updater.sh $DIST/

#dist.php is special. it lives in the parent of $DIST,
# and it becomes index.php in our release directory:
#echo -en "  - "
#cp -v $DISTDIR/dist.php $DIST/index.php

#cameraleash.html is likewise special. it lives in dist/
# and doesn't go into tarballs
#echo -en "  - "
#cp -v $DISTDIR/cameraleash.php $DIST/

#copy screenshots:
#for f in $DISTDIR/screenshot*.jpg; do
#	echo -en "  - "
#	cp -v $f $DIST/
#done;

#finally, we build a release tarball to go into $WWW
TARBALL=$WWW/latest.tar.bz2
echo "* Building tarball '$TARBALL'..."
tar jcf $TARBALL --directory "`dirname $DIST`" cameraleash

#copy our version manifest file into the dist
echo "* writing manifest '$WWW/latest'..."
cp $SUMFILE $WWW/latest
#need to give everybody read access to this:
chmod a+r $WWW/latest

rm $SUMFILE

