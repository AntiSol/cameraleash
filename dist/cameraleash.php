<?php

/*
 * This is the cameraleash website. it's included by index.php if the request is not a POST
 */
$basepath = dirname(__FILE__);
$latest = trim(file_get_contents($basepath . '/VERSION'));

?>
<html>
<head>
	<style>
		body {
			background: #000;
			color: #33ff33;
			font-family: monospace;
		}
		
		div.screenshots {
			width: 100%;
		}
		div.screenshots:after {
			content: "";
			display: block;
			clear: both;
			width: 100%;
			height: 0px;
		}
		div.screenshots > a > img {
			width: 30%;
			float: left;
			margin-right: 1em;
		}
		
	</style>
	<title>CameraLeash Project</title>
</head>
<body>

	<h1>CameraLeash</h1>
	<p>
	CameraLeash is a pygtk program which allows you to control your DSLR
	camera using the gphoto2 library. It is built for the OpenPandora and
	therefore optimised to run on small-screen devices with low resources.
	</p>
	
	<h2>Screenshots</h2>
	<div class="screenshots">
	<?php
		/**
		 * list of screenshots.
		 */
		$files = glob($basepath . "/screenshot*.jpg");
		foreach ($files as $file) {
			$file = basename($file);
			echo("<a href='$file' target='_blank'><img src='$file' /></a>");
		}
	?>
	</div>
	<div>
	<h2>Features</h2>
	<p>
		CameraLeash has a bunch of nifty features:
		<ul>
			<li>Live video preview. Autofocus</li>
			<li>Image capture and image sequence capture. Downloads both raw and jpg if capturing in that mode.</li>
			<li>Browse and download files on device</li>
			<li>Ability to detect camera settings and configure display
				to only show relevant settings.</li>
			<li>Auto-Update System</li>
			<li>(coming soon) scripting support, enabling complex
				actions.</li>
		</ul>
	</p>
	
	<h2>Thanks</h2>
	<ul><li>Thanks to Jim Easterbrook for his <a href="https://github.com/jim-easterbrook/python-gphoto2/" target="_blank">python-gphoto2</a> library. Thanks in particular to Jim for following up very quickly for me and fixing a couple of issues.</li>
		<li>Thanks to Ptitseb on the openpandora forums, for packaging gphoto2 and the relevant python libraries for the pandora for me. I use a version of gphoto2 and the python bindings provided by ptitseb in my OpenPandora release.</li>
	</ul>
	
	
	<h2>Downloads</h2>
	<p>
	The latest version of CameraLeash is <?php echo($latest); ?>. 
	<a href="latest.tar.bz2" target="_blank">Download it</a>.<br />
	Or, <a href="gphoto2.pnd" target="_blank">Download the PND for OpenPandora</a>.<br />
	Or, <a href="https://gitlab.com/AntiSol/cameraleash" target="_blank">Clone the git repo</a>.
	
	</p>
	
	<hr />
	<h2>Version History</h2>
	<pre>
	<?php echo(file_get_contents($basepath .'/RELEASES')); ?>
	</pre>
	</div>
</body>
</html>
