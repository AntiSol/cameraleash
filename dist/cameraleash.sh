#!/bin/bash

. gphoto2.sh

cd cameraleash

if [ -z "`lsmod | grep ehci_hcd`" ]; then
	#USB Host mode is off, toggle it:
	if [ -x "/usr/pandora/scripts/op_usbhost.sh" ]; then
		echo "USB Host mode is off, enabling it..."
		sudo /usr/pandora/scripts/op_usbhost.sh
	fi
fi

python cameraleash.py

