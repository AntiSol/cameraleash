#!/bin/bash

#this is a 'custom files' bash script which is run during 'make dist'
# it contains custom copy commands which don't fit into the 
# (dist|www)_files.txt scheme, e.g if you want to copy and rename.
# put a comment explaining what you're doing.

#dist/dist.php is our updater php script, which has all the smarts
# for comparing checksums given by clients and building a
# differential tarball. it needs to become www/index.php.
# it's not called index.php to avoid people making the dist dir
# public.
cp -v dist/dist.php dist/www/index.php

