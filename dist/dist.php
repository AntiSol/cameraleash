<?php

/**
 * DM's Auto-updater system, web component.
 *
 * This is a simple web page which site in a 'dist' directory. it
 * takes manifests (a version number followed by a list of files and
 *	checksums) from clients and checks them against current checksums,
 *	optionally generating a tarball containing only the changed files,
 *	and returning a response containing information (like release notes)
 */
$appname = "CameraLeash";
$basepath = dirname(__FILE__);
$manifest = $basepath . '/latest';

if (
		!isset($_SERVER['REQUEST_METHOD']) || 
		($_SERVER['REQUEST_METHOD'] !== 'POST') //||
		/*
		!isset($_POST['manifest']) ||
		!$_POST['manifest']
		*/
) {
	//The request is not a POST, output the project website.
	include('cameraleash.php');
	exit();
}

//read manifest:
$mfdata = file_get_contents($manifest);


function parse_manifest($data) {
	$ret = Array();
	$version = null;
	foreach (explode("\n",$data) as $line) {
		if ($version === null) {	//first line, read version:
			$version = $line;
			$ret['version'] = floatval($version);
			continue;
		}
		
		$line = trim($line);
		
		if ($line) {
			if ($line[0] === "#") continue;	//comments
			
			list($hash,$fn) = explode(" ",$line,2);
			$fn = trim($fn);
			$hash = trim($hash);
			$ret[$fn] = $hash;
		}
	}
	return $ret;
}

function hunam_filesize($filename, $decimals = 2) {
  $sz = 'BKMGTP';
  $bytes=filesize($filename);
  $factor = floor((strlen($bytes) - 1) / 3);
  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . " " . @$sz[$factor];
}

if (!isset($_POST['manifest'])) {
	//POST must have a 'manifest' parameter, with the manifest data as value.
	//curl command to post current manifest:
	//curl --data-urlencode manifest@/path/to/dist/latest
	header("HTTP/1.0 400 Bad Request",true,400);
	die("Bad Request");
}

//parse the current manifest:
$current = parse_manifest($mfdata);

//parse manifest provided by the client:
$client = parse_manifest($_POST['manifest']);

//look for files with different checksums:
$changed = Array();

$files = "";
$key = "";

foreach ($current as $file => $hash) {
	
	if ( !isset($client[$file]) || 	//new file, not in client manifest
		($client[$file] !== $hash)	//checksum mismatch
	) {
		$changed[] = $file;
		$key="$file=$hash:$key";
		$files .= ($files?" ":"") . $file;
	}
	
}

if ($changed && ($current['version'] >= $client['version'])) {
	//one or more files does not match, update required
	//we aso check version numbers - if the client has a higher 
	//  version (e.g: dev), don't try to "upgrade" it
	// we do allow the same version as this could work to fix 
	// corrupt installs
	
	//key hash
	$kh = sha1($key);

	//'which tar'
	$tar = trim(`which tar`);
	if (!$tar) die("tar is not installed!");

	//path to tarball
	$tarball = "$basepath/tarballs/$kh.tar.bz2";
	if (!file_exists($tarball)) {
		//file does not already exist
		
		//build a tar command to create the tarball
		$tar = "$tar jcf $tarball --directory $basepath $files";
		
		//build the tarball
		//error_log("Creating tarball '$tarball' using '$tar'");
		system($tar);
	}
	
	$url = "tarballs/$kh.tar.bz2";

	$size = hunam_filesize($tarball);

	//we output a bash script which can be sourced, for lack of a better
	//	option.
	echo("STATUS=UPDATE\n");
	echo("URL='$url'\n");
	echo("SIZE='$size'\n");
	echo("FILES='" . json_encode(implode("\n",$changed)) . "'\n");
	$notes = "\nA new version of $appname (v" . $current['version'] . ") is available, Do you want to update?\n" . str_repeat("-",80) . "\n" .
		"This download is $size and will modify the following file(s):\n - " . implode("\n - ",$changed) . "\n" .
		str_repeat("-",80) . "\n" .
		str_replace("'","",file_get_contents($basepath . "/RELEASES"));
	echo("NOTES='" . json_encode($notes) . "'\n");
	
	
} else {
	//using the latest version
	echo("STATUS=");
}

//echo json_encode($output,JSON_PRETTY_PRINT);
