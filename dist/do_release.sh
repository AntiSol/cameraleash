#!/bin/bash

if [ -n "`git diff`" ]; then
	echo -e "\nit looks like there are changes on the git branch:"
	git diff --stat=80
	echo -e "\nYou should not do a release with a dirty branch,"
	echo -e "  please e.g commit, stash, or checkout.\n"
	echo -en "\nAbort (Y/N)? "
	read yn
	if [[ "$yn" =~ [Nn][oO]? ]]; then
		echo -e "\n"
	else
		exit 2
	fi
fi

if [ -z "$EDITOR" ]; then	#this should always be set, right?
	EDITOR=nano
fi

echo "To do a release, you should update the VERSION file. Do that now?"
read yn
if [[ $yn =~ ^[Yy] ]]; then
	$EDITOR VERSION
fi

# do a release
VERSION=`cat VERSION`

echo  -n "About to do release v$VERSION. Proceed (y/n)? "
read yn
if [[ $yn =~ ^[Nn] ]]; then
	echo "Aborting."
	exit 1
fi

#we need to start in the project root:
cd `dirname $0`/../

DISTFILE="dist.$VERSION.tar.bz2"
RELFILE="release.$VERSION.tar.bz2"
PNDFILE="release.$VERSION.pnd"

echo " - doing release..."
make dist

echo " - preparing dist tarball $DISTFILE..."
cd dist

tar jcf releases/$DISTFILE --exclude=tarballs cameraleash
cp -v releases/$DISTFILE cameraleash/latest.tar.bz2
echo " - copying release tarball $RELFILE..."
cp cameraleash/latest.tar.bz2 releases/$RELFILE

echo " - preparing PND release $PNDFILE..."
make pnd
cp -v gphoto2.pnd.new releases/$PNDFILE

#create 'latest' symlinks for pnd and tarball:
cd releases
rm -f latest.tar.bz2 latest.pnd gphoto2.pnd 2>/dev/null
ln -s $RELFILE latest.tar.bz2
ln -s $PNDFILE gphoto2.pnd

cd ../..

echo "dist file is: '$DISTFILE'"
echo "release file is: '$RELFILE'"
echo "PND file is: '$PNDFILE'"
