#!/bin/bash

# updater script. Downloads the new version from the
# internet, unpacks it, and restarts the program.

#new method: be a bit more clever. Grab version number and URL from files.
# we then build a manifest and POST that to our URL. This gives us back
# a bunch of info we can show the user, e.g release notes. It also gives
# us back a tarball containing only the changed files. We show the user
# a nice dialog offering to update.

# curl command for posting a file as the 'manifest' parameter:
# curl --data-urlencode manifest@/tmp/latest 

DISTFILE=dist_files.txt
URL=`cat URL`
BASEURL=$URL

#used for debug output
DEBUG=False

BASEPATH=`dirname $0`

#create temp file for our manifest:
MANIFEST=`mktemp /tmp/manifest.XXXX`

#build manifest:
cat VERSION >$MANIFEST
for f in `cat $DISTFILE`; do
	#we do basename because files can come from multiple
	# directories but they're all placed in /
	f=`basename $f`
	sha1sum $f >>$MANIFEST
done

#temp file for query response:
RESULT=`mktemp /tmp/result.XXXX`

if [ "$DEBUG" == True ]; then
	cp -v $MANIFEST /tmp/manifest

	echo "curl --data-urlencode manifest@$MANIFEST $URL >$RESULT 2>/dev/null"
fi

#send manifest to the server:
curl --data-urlencode manifest@$MANIFEST $URL >$RESULT 2>/dev/null

#we source the result, which gives us a few variables:
#	$STATUS		'UPDATE' if there's an update.
#	$URL		url for download link
#	$NOTES		text for update dialog.
. $RESULT

do_update() {
	#do an update. download $URL to a temp file and unpack it
	url="$BASEURL$URL"
	echo "Downloading '$url'..." >&2
	TARBALL=`mktemp /tmp/tarballXXXXXXX`
	curl -o $TARBALL $url 2>/dev/null
	#cd $BASEPATH
	echo "Updating software..." >&2
	gksudo --description "CameraLeash Updater" "bash -c 'cd $BASEPATH;tar xf $TARBALL'" 2>/tmp/update.msg
	RES="$?"
	if [ ! "$RES" ]; then
		zenity --warning --text "An error occurred trying to update CameraLeash. Messages\n\n`cat /tmp/update.msg`"
	fi
	#cleanup
	rm $TARBALL
}

if [ "$STATUS" == "UPDATE" ]; then
	#show dialog, look at response (OK/cancel):
	NOTES=${NOTES#*\"}
	NOTES=${NOTES%\"*}
	echo -e $NOTES | zenity --text-info --width 640 --height 320 --title "New Version Available"
	RESP=$?
	if [ "$RESP" == 0 ]; then
		do_update | zenity --progress --pulsate --text "Updating..." --title "Updating" --auto-close --no-cancel
		#do_update

		zenity --info --text "Update complete. You should restart the program to use the new version." --title "Update Complete"

		#zenity --info --text "Doing Update..."
		#TODO:
		# wget $URL. probably with an output file in a temp directory.
		# decompress the tarball, overwriting files in current directory.
		# display a message advising to restart the program.
		#
		
	else
		echo "Update cancelled."
	fi
else
	echo "You are using the latest version."
fi

#remove temp files
rm $MANIFEST $RESULT
