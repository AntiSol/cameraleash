from gui import *

import gphoto2 as gp

from configwidget import ConfigWidget

class CamCtrlLeftPaneConfigWidget(gtk.ScrolledWindow):
	"""
	The configurator for the left pane of the program.
	Shows a list of config items from the camera with configurable 
		label, visibility, and type for each
	"""
	def __init__(self,camera,config):
		gtk.ScrolledWindow.__init__(self)
		self.set_border_width(5)
		self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		
		self.camera = camera
		self.config = config
		#columns: item name, item type, description, visible, type, type description
		
		self.store = gtk.ListStore(str,int,str,bool,str,str)
		
		self.treeview = gtk.TreeView(self.store)
		self.treeview.set_reorderable(True)
		self.add(self.treeview)
		
		column = gtk.TreeViewColumn('Name')
		#column.set_resizable(True)
		self.treeview.append_column(column)
		
		cell = gtk.CellRendererText()
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 0)
		column.set_expand(True)
		
		
		column = gtk.TreeViewColumn('Label')
		#column.set_resizable(True)
		self.treeview.append_column(column)
		
		cell = gtk.CellRendererText()
		cell.set_property('editable', True)
		cell.connect('edited',self.label_changed)
		
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 2)
		column.set_expand(True)
		
		column = gtk.TreeViewColumn('Active')
		self.treeview.append_column(column)
		cell = gtk.CellRendererToggle()
		cell.set_activatable(True) 
		cell.connect('toggled', self.active_toggled) 
		column.pack_start(cell, True)
		column.add_attribute(cell, 'active', 3)
		
		column = gtk.TreeViewColumn('Type')
		self.treeview.append_column(column)
		cell = gtk.CellRendererCombo()
		cell.set_property('editable', True)
		cell.set_property("text-column", 1)
		cell.set_property('has-entry',False)
		
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 5)
		self.combobox = cell
		self.combobox.connect('edited',self.type_changed)
		
		selection = self.treeview.get_selection()
		selection.connect('changed',self.populate_combo)
		
		"""
		A callback to fire when something changes.
		Callback should accept one parameter - the LeftPaneConfigWidget
		"""
		self.on_change = None
		
		self.change_handler = None
		self.reorder_handler = None
		
		self.populate()
		
	def label_changed(self,cellrenderertext, path, new_text):
		iter = self.store.get_iter(path)
		self.store.set_value(iter,2,new_text)
		
	def row_changed(self,model, path, iter):
		"""
		Callback when a row changes. 
		Note that you might get multiple row_changed events for one action,
			e.g changing a control type changes two rows (type and description)
		"""
		#fire a callback provided by the parent
		if self.on_change:
			self.on_change(self)
			
		
	def active_toggled(self,widget,path):
		iter = self.store.get_iter(path)
		self.store.set_value(iter,3,not self.store.get_value(iter,3))
		
	def type_changed(self, widget, listpath, text):
		"""
		Callback for when type dropdown chcanges value
		"""
		
		model = self.combobox.get_property('model')
		
		"""
		Despite what the pygtk documentation says, the third argument to 
		this callback is the newly-selected text in the combo, not an iter.
		I can't seem to find a way to get an iter for the currently selected
		row, so we're going to have to go through them all until we find
		matching text. Le sigh.
		"""
		iter = model.get_iter_first()
		while iter:
			if model.get_value(iter,1) == text:
				break
			iter = model.iter_next(iter)
		
		
		type = model.get_value(iter,0)
		typedesc = model.get_value(iter,1)
		listiter = self.store.get_iter(listpath)
		self.store.set_value(listiter,4,type)
		self.store.set_value(listiter,5,typedesc)
		
	def populate_combo(self,selection):
		"""
		Populate the combo box for the selected row. 
		Called when the selection changes
		"""
		(model, paths) = selection.get_selected_rows()
		if not paths:
			#nothing selected, can't populate'
			return False
			
		for path in paths :
			tree_iter = model.get_iter(path)
			ctrltype = model.get_value(tree_iter,4)
			cfgtype = model.get_value(tree_iter,1)
			itm = model.get_value(tree_iter,0)
			break
			
		combolist = gtk.ListStore(str,str)
		
		cfg = self.camera.config.all
		if itm in cfg:
			#self.combobox.set_sensitive(True)
			valid = ConfigWidget.types_for(cfgtype)
		else:
			#item does not exist for current camera, can't change type
			# because we don't know the config type, so we fake it by
			# only populating the combo with the control type from 
			# widget config
			valid = [ctrltype]
		
		for itm in valid:
			combolist.append([itm,ConfigWidget.describe_type(itm)])
		
		self.combobox.set_property("model", combolist)
		
		
	def populate(self):
		"""
		Populate the contents of the list
		"""
		
		#we don't want 'changed' callbacks while populating:
		if self.change_handler:
			self.store.disconnect(self.change_handler)
		
		if self.reorder_handler:
			self.store.disconnect(self.reorder_handler)
		
		self.store.clear()
		
		cfg = self.camera.config.all
		
		# a list of which config items we've used, so that we don't
		# include them twice when listing config items from the camera
		used = []
		
		#first we add items for our config:
		for ctrl in self.config:
			itm = ctrl['item']
			used.append(itm)
			#if itm not in cfg:
			#	#invalid item
			#	continue
			
			if 'type' in ctrl:	
				type = ctrl['type']
			else: 
				type = ConfigWidget.default_for(self.camera.config.typeof(itm))
			
			if 'label' in ctrl:
				label = ctrl['label']
			else:
				label = self.camera.config.describe(itm)
				
			typedesc = ConfigWidget.describe_type(type)
			
			cfgtype = self.camera.config.typeof(itm)
			if not cfgtype: cfgtype = gp.GP_WIDGET_TEXT	#this is not right, but it doesn't matter because we won't allow changing type
			
			self.store.append([itm,cfgtype,label,True,type,typedesc])
			
		for itm in cfg:
			if itm in used: 
				continue #no duplicates
				
			type = ConfigWidget.default_for(self.camera.config.typeof(itm))
			label = self.camera.config.describe(itm)
			typedesc = ConfigWidget.describe_type(type)
			
			cfgtype = self.camera.config.typeof(itm)
			
			self.store.append([itm,cfgtype,label,False,type,typedesc])
		
		#fire callback if something changed	
		self.change_handler = self.store.connect('row-changed',self.row_changed)
		#self.reorder_handler = self.store.connect_after('rows-reordered',self.row_changed)
		
			
	def new_config(self):
		"""
		Returns a dict which can be used in CamCtrlMainWindow.camera_control_config
		Based on current state of the control
		"""
		items = []
		ret = []
		iter = self.store.get_iter_first()
		while iter:
			if self.store.get_value(iter,3): 
				#item is active
				itm = {}
				item = self.store.get_value(iter,0)
				if item not in items: #no duplicates
					itm['item'] = item
					itm['type'] = self.store.get_value(iter,4)
					itm['label'] = self.store.get_value(iter,2)
					ret.append(itm)
					items.append(item)
			iter = self.store.iter_next(iter)
		#print ret
		return ret
		
		
