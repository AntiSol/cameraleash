import unittest
import os
import sys

basedir = os.path.dirname(os.path.abspath(__file__))

sys.path.insert(0,basedir)

loader = unittest.TestLoader()

tests = loader.discover(os.path.join(basedir,"test"),"*.py",basedir)

print "-" * 70
print "Running %1d Tests..." % tests.countTestCases()
print "-" * 70

result = unittest.TestResult()
#tests.run(result,False)

result = unittest.TextTestRunner(verbosity=3).run(tests)

#print result

