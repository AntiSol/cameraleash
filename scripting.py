from gui import *
import logging
import gphoto2 as gp
#from bettercamera import BetterCamera

class ScriptEngine(object):
	"""
	Object for parsing, running, and creating scripts.
	"""
	def __init__(self,parent = None):
		"""
		We can only do a limited subset of validation if we don't 
			have a current camera since we can't check config 
			items for validity.
		Ideally, scripts are associated with a camera.
		"""
		self.__parent = parent
		self.__camera = None
		if parent.camera:
			self.__camera = camera
		self.__loops = {} #name: iterations remaining
		
		"""
		how we store a script: a list of commands
		each of these is a tuple containing: (command, ([arg][,arg, ... ]))
		"""
		self.__script = []
		
	def __repr__(self):
		"""
		Return a text representation of the script
		"""
		ret = "# CameraLeash Script."
		for itm in self.__script:
			cmd,args = itm
			if ret:
				ret += "\n"
			ret += ("%s" % cmd) + " ".join(args)
		ret += "\n"
		
		return ret
		#return "A script with %1d commands" % len(self.__script)
	
	def run_command(self,command,args = []):
		"""
		Run the specified command with the provided args
		"""
		fn_n = "cmd_%s" % command
		if hasattr(self,fn_n):
			#valid command.
			fn = getattr(self,fn_n)
			return fn(args)
		return False
		
	def parse_line(self,line):
		"""
		parses a line of text into a command and args, 
		returning something like ('cmd',[arg1,...])
		"""
		cmd = None
		args = []
		line = line.strip()
		if line[0:1] == "#":
			#comment
			return None
		
		words = line.split(" ")
		
		quoted = False
		arg = ""
		
		for i,v in enumerate(words):
			if i == 0:
				cmd=v.upper()
			else:
				#support quoted args. Splitting on space will break
				#		for a command like 'set somesetting "some value"'
				if (v[0] in [ '"', "'" ]) and not quoted:
					#start quoting
					quoted = True
					arg=v[1:]
					v=None
				
				elif (v[-1] in [ '"', "'" ]) and quoted:
					#end quoting
					quoted = False
					v = arg + " " + v[0:-1]
					arg=""
				
				if v != None:
					args.append(v)
					
		if arg: #no ending quote
			args.append(arg)
		
		ret= (cmd,args)
		return ret
		
	def parse_text(self,text):
		"""
		Parse text into the current script.
		"""
		lines = "\n".split(text)
		script = []
		for line in lines:
			cmd = parse_line(line)
			if cmd:
				script.append(cmd)
				
		self.__script = script
		return True
		
	def validate_command(self,command,args = []):
		"""
		Return a string containing validation errors for the provided
		command and arguments. 
		If we have a camera connected, this includes checking
		validity of config items where possible. Note that this doesn't
		guarantee that a script will actually work!
		return is a string containing errors. if return is "", 
		validation is ok
		"""
		lst = self.commandlist()
		if command not in lst:
			return "Invalid command '%s'" % commandlist
			
		fargs = self.args(command)
		
		if len(args) != len(fargs):
			return "'%s' requires %1d arguments, got %1d" % (
				command,len(fargs),len(args)
			)
		
		fn_n = "validate_%s" % command 
		if hasattr(self,fn_n):
			#run validator
			fn = getattr(self,fn_n)
			return fn(args)
			
		return ""
		
		
	def commandlist(self):
		"""
		Get a list of commands from the class, along with descriptions
		"""
		ret = {}
		for i in dir(self):
			if i.startswith("cmd_"):
				#commnd
				cmd = i[4:]
				desc = "Unknown"
				if hasattr(self,"desc_%s" % cmd):
					itm = getattr(self,"desc_%s" % cmd)
					desc = itm()
				ret[cmd] = desc
		
		return ret
	
	def args(self,command):
		"""
		Get arguments for a command
		"""
		if hasattr(self,"args_%s" % command):
			fn = getattr(self,"args_%s" % command)
			return fn()
		return None
	
	"""
	Scripting engine commands are methods that begin with cmd_
	the command will be the following text in the method name
	the engine will enumerate these methods and provide validation
	
	method desc_<name> should return a string describing the command
	
	method validate_<name> should validate arguments passed to it to 
			determine is a call to cmd_<name> is valid.
			Returns a string with error message(s), or an empty string
			if valid.
	
	methos args_<name> describes arguments for a command
	
	"""
	def cmd_set(self,args):
		"""
		Set a new value for a config item
		"""
		if len(args) != 2:
			logging.error("not enough args provided to set: %s" % args)
			return False
		
		item, val = args
		
		camera.config[item] = val
		
		return True
		
	def desc_set(self):
		return "Set a camera config item. Requires 2 parameters: item and value."
	def args_set(self):
		return {
			'item': 'Name of the config item to set, e.g "f-number"',
			'value': 'New value for config item. e.g "f/3.5"'
		}
	def validate_set(self,args):
		"""
		If we have a camera, first arg should be a valid config item,
			and second arg should be a valid value for that item
		return error message(s) as string or "" if valid.
		"""
		if len(args) != 2:
			logging.info("not enough args provided to set: %s" % args)
			return "Wrong Number of Arguments"
		
		if not self.__camera:
			#can''t validate
			return ""
		
		item,val = args
		if item in self.__camera.config.all:
			choices = self.__camera.config.choices(item)
			if choices:
				#menu/radio, validate choice
				if val in choices:
					return ""
				else:
					return "'%s' is not a valid choice for '%s'" % (val,item)
			else:
				#not a radio/menu. Ideally we should check range/numeric
				# values here, but that sounds difficult.
				return ""
		else:
			return "'%s' is not a valid config item" % item
		
	
	def cmd_wait(self,args):
		"""
		Wait for interval seconds...
		"""
		if len(args) != 1:
			logging.error("not enough args for wait")
			return False
			
		end = time.time() + float(args[0])
		while time.time() < end:
			#gui update:
			self.__parent.gui_update()
			time.sleep(0.1)
		
		return True
		
	def desc_wait(self):
		return "Wait for a specified interval of time. Requires one parameter: wait time in seconds"
	def args_wait(self):
		return {
			'interval': 'Number of seconds to pause'
		}
	def validate_wait(self,args):
		if len(args) != 1:
			return "Not enough args provided to wait"
		#check is numeric?
		return ""
		
	def cmd_capture(self,args):
		"""
		Capture a shot and download it
		No config
		"""
		self.__parent.capture()
		
	def desc_capture(Self):
		return "Caoture and download an image"
		
		
	def cmd_loop(self,args):
		"""
		Create a loop with name, repeating count times.
			-1 means forever
		"""
		
		if len(args) != 2:
			logging.error("Wrong number of argumsnts for loop")
		name,num = args
		self.__loops[name] = num
		
	def desc_loop(self):
		return "Create a loop with a name and number of iterations"
	def args_loop(self):
		return {
			'name': 'The name of the loop',
			'iterations': 'Number of iterations. -1 for infinite.'
		}
	
	def cmd_end(self,args):
		"""
		End an iteration of loop with the given name
		"""
		pass
	def desc_end(self):
		return "End a loop, iterating back to the start if appropriate"
	def args_end(self):
		return {
			'name': 'Name of the loop to iterate. Must have been previously defined with loop(name).'
		}
		
	
	def step(self):
		"""
		Step through the sript, i.e run one command
		this yields 
		"""
		pass
		
	def run(self):
		"""
		run script
		"""
		pass
		
	
	@staticmethod
	def from_text(text,camera = None):
		"""
		Parse text into a script object
		"""
		obj = ScriptEngine(camera)
		obj.parse_text(text)
		return obj
		
	@staticmethod
	def load(script_file,camera = None):
		"""
		Load a script file
		"""
		pass
		
		
		
	
class CameraScriptEditor(gtk.VBox):
	def __init__(self,parent):
		gtk.VBox.__init__(self)
		self.__parent = parent
		#cfg = self.__parent.config.all
		cfg = {}
		self.filename = GtkLeftAlignLabel("No File Loaded")
		self.pack_start(self.filename,False)
		hb = gtk.HBox()
		self.load_script_button = gtk.Button("Load")
		self.save_script_button = gtk.Button("Save")
		self.run_script_button = gtk.Button("Run")
		hb.pack_start(self.load_script_button,True,True,3)
		hb.pack_start(self.save_script_button,True,True,3)
		hb.pack_start(self.run_script_button,True,True,3)
		self.pack_start(hb,False)
		#now we have a scrolledwindow which holda a bunch of ScriptActions
		
		#self.pack_start(CameraScriptAction(self.__parent),False,True,3)
		
		self.textbox = GtkMultiLineTextBox()
		self.pack_start(self.textbox,True,True,3)
		
		self.show_all()
		
class CameraScriptAction(gtk.HBox):
	def __init__(self,parent,parsetext = None):
		"""
		A series of controls allowing the user to specify one camera action.
		This equates to one line in the script.
		"""
		gtk.HBox.__init__(self)
		self.__parent = parent
		self.action_cb = gtk.combo_box_new_text()
		self.action_cb.append_text("Set")
		self.action_cb.append_text("Wait")
		self.action_cb.append_text("Capture")
		self.action_cb.connect('changed',self.action_changed)
		self.pack_start(self.action_cb)
		self.paramsbox = gtk.HBox()
		self.pack_start(self.paramsbox)
		
	def action_changed(self,widget):
		for c in self.paramsbox.get_children():
			self.paramsbox.remove(c)
			
		if widget.get_active_text() == "Set":
			if self.__parent.camera:
				cfg = self.__parent.camera.config
				cb_configitem = gtk.combo_box_new_text()
				for i in cfg.all:
					if cfg.writable(i):	#only writable
						cb_configitem.append_text(i)
				cb_configitem.connect('changed',self.item_changed)
				self.paramsbox.pack_start(cb_configitem)
				
			
		elif widget.get_active_text() == "Wait":
			
			btn = gtk.SpinButton()
			self.paramsbox.pack_start(btn)
			
			lbl = GtkLeftAlignLabel("Seconds")
			self.paramsbox.pack_start(lbl,True,3)
			
		self.paramsbox.show_all()
		
	def item_changed(self,widget):
		itm = widget.get_active_text()
		choices = self.__parent.camera.config.choices(itm)
		if choices:
			"""
			There are choices, make it a combobox
			"""
			cb_choice = gtk.combo_new_text()
			for i in choices:
				cb_choice.append_text(i)
			self.paramsbox.pack_start(cb_choice)
		else:
			tx_val = gtk.Entry()
			self.paramsbox.pack_start(tx_val)
			
		self.paramsbox.show_all()
