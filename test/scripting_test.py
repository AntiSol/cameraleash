import unittest

from scripting import ScriptEngine

class ScriptingTest(unittest.TestCase):
	
	def setUp(self):
		self.engine = ScriptEngine()
	
	"""
	A dict with a list of tests for test_parse_line.
	The key is the line to be parsed, value is what it should parse to
	"""
	parsertests = {
		
		'Set someitem some thing': ("SET",["someitem", "some","thing"]),
		
		#test quoted arguments:
		'Set someitem "some thing"': ("SET",["someitem", "some thing"]),
		#single quote works too
		"Set someitem 'some thing'": ("SET",["someitem", "some thing"]),
		
		#a string which starts with a quote is considered quoted, and the quote
		#	will be stripped, even if the quote is not closed - as per the last
		#	argument here
		"set someitem some' 'thing": ("SET",["someitem", "some'","thing"]),
		
	}
	
	def do_parse_line_test(self,line,expected):
		"""
		Perform a single test of parse_line
		"""
		parsed = self.engine.parse_line(line)
		print(" - parse_line(%s) -> %s" % (line,parsed))
		self.assertEquals(parsed,expected)
	
	def test_parse_line(self):
		"""
		Iterate over self.parsertests and run them all
		"""
		print("")
		for k in self.parsertests:
			v = self.parsertests[k]
			self.do_parse_line_test(k,v)



